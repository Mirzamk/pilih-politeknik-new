<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Map1;
use App\Models\Pemetaan;
use App\Models\Pemetaan2;
use Illuminate\Support\Facades\DB;

class Map1Controller extends Controller
{
    public function index()
    {
        if (empty($_GET['kuisioner'])) {
            $kuisioner = 'kuisioner1';
            $id = 'id_kuisioner1';
        } else {
            $kuisioner = 'kuisioner' . $_GET['kuisioner'];
            $id = 'id_kuisioner' . $_GET['kuisioner'];
        }
        
        $db_kuisioner = array();
        $daftarKota = DB::table($kuisioner)->select('kota')->groupBy('kota')->get()->toArray();
        // dd($daftarKota);
        foreach ($daftarKota as $objekKota) {
            $kota = $objekKota->kota;
            $kuisionerKota = DB::table($kuisioner)->select(DB::raw("(count(" . $id . ")) as total_responden"), 'politeknik')->groupBy('politeknik')
                ->where('kota', \strtolower($kota))
                ->orWhere('kota', \strtoupper($kota))
                ->orWhere('kota', \ucfirst(\strtolower($kota)))->get()->toArray();
            $max = 0;
            foreach ($kuisionerKota as $key => $poli) {
                if ($poli->total_responden > $kuisionerKota[$max]->total_responden) {
                    $max = $key;
                }
            }
            $kuisionerKota['major'] = $kuisionerKota[$max]->politeknik;

            $db_kuisioner[$kota] = $kuisionerKota;
        }

        $lamongan = DB::table('kuisioner1')
                    ->where('kota', 'lamongan')
                    ->count();
        $lamongan2 = DB::table('kuisioner2')
                    ->where('kota', 'lamongan')
                    ->count();
        $lamonganMerge = $lamongan + $lamongan2;
        
        $sumenep = DB::table('kuisioner1')
                    ->orWhere('kota', 'sumenep')
                    ->count();
        $sumenep2 = DB::table('kuisioner2')
                    ->orWhere('kota', 'sumenep')
                    ->count();
        $sumenepMerge = $sumenep + $sumenep2;

        $surabaya = DB::table('kuisioner1')
                    ->where('kota', 'surabaya')
                    ->count();
        $surabaya2 = DB::table('kuisioner2')
                    ->where('kota', 'surabaya')
                    ->count();
        $surabayaMerge = $surabaya + $surabaya2;

        $kediri = DB::table('kuisioner1')
                    ->where('kota', 'kediri')
                    ->count();
        $kediri2 = DB::table('kuisioner2')
                    ->where('kota', 'kediri')
                    ->count();
        $kediriMerge = $kediri + $kediri2;

        $pamekasan = DB::table('kuisioner1')
                    ->where('kota', 'pamekasan')
                    ->count();
        $pamekasan2 = DB::table('kuisioner2')
                    ->where('kota', 'pamekasan')
                    ->count();
        $pamekasanMerge = $pamekasan + $pamekasan2;

        $bangkalan = DB::table('kuisioner1')
                    ->where('kota', 'bangkalan')
                    ->count();
        $bangkalan2 = DB::table('kuisioner2')
                    ->where('kota', 'bangkalan')
                    ->count();
        $bangkalanMerge = $bangkalan + $bangkalan2;

        $madiun = DB::table('kuisioner1')
                    ->where('kota', 'madiun')
                    ->count();
        $madiun2 = DB::table('kuisioner2')
                    ->where('kota', 'madiun')
                    ->count();
        $madiunMerge = $madiun + $madiun2;

        $sidoarjo = DB::table('kuisioner1')
                    ->where('kota', 'sidoarjo')
                    ->count();
        $sidoarjo2 = DB::table('kuisioner2')
                    ->where('kota', 'sidoarjo')
                    ->count();
        $sidoarjoMerge = $sidoarjo + $sidoarjo2;
        
        $lamonganPens = DB::table('kuisioner1')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'pens')
                    ->count();
        $lamonganPens2 = DB::table('kuisioner2')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'pens')
                    ->count();
        $lamonganPensCA = DB::table('kuisioner3')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'pens')
                    ->count();
        $lamonganPensMerge = $lamonganPens + $lamonganPens2;

        $lamonganPpns = DB::table('kuisioner1')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $lamonganPpns2 = DB::table('kuisioner2')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'Ppns')
                    ->count();
        $lamonganPpnsCA = DB::table('kuisioner3')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'Ppns')
                    ->count();
        $lamonganPpnsMerge = $lamonganPpns + $lamonganPpns2;

        $lamonganPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $lamonganPolinema2 = DB::table('kuisioner2')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $lamonganPolinemaCA = DB::table('kuisioner3')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $lamonganPolinemaMerge = $lamonganPolinema + $lamonganPolinema2;

        $lamonganPolije = DB::table('kuisioner1')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polije')
                    ->count();
        $lamonganPolije2 = DB::table('kuisioner2')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polije')
                    ->count();
        $lamonganPolijeCA = DB::table('kuisioner3')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polije')
                    ->count();
        $lamonganPolijeMerge = $lamonganPolije + $lamonganPolije2;
        
        $sumenepPens = DB::table('kuisioner1')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'pens')
                    ->count();
        $sumenepPens2 = DB::table('kuisioner2')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'pens')
                    ->count();
        $sumenepPensCA = DB::table('kuisioner2')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'pens')
                    ->count();
        $sumenepPensMerge = $sumenepPens + $sumenepPens2;
        
        $sumenepPpns = DB::table('kuisioner1')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'ppns')
                    ->count();
        $sumenepPpns2 = DB::table('kuisioner2')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'ppns')
                    ->count();
        $sumenepPpnsCA = DB::table('kuisioner3')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'ppns')
                    ->count();
        $sumenepPpnsMerge = $sumenepPpns + $sumenepPpns2;

        $sumenepPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'polinema')
                    ->count();
        $sumenepPolinema2 = DB::table('kuisioner2')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'polinema')
                    ->count();
        $sumenepPolinemaCA = DB::table('kuisioner3')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'polinema')
                    ->count();
        $sumenepPolinemaMerge = $sumenepPolinema + $sumenepPolinema2;

        $sumenepPolije = DB::table('kuisioner1')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'polije')
                    ->count();
        $sumenepPolije2 = DB::table('kuisioner2')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'polije')
                    ->count();
        $sumenepPolijeCA = DB::table('kuisioner3')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'polije')
                    ->count();
        $sumenepPolijeMerge = $sumenepPolije + $sumenepPolije2;

        $surabayaPens = DB::table('kuisioner1')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'pens')
                    ->count();
        $surabayaPens2 = DB::table('kuisioner2')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'pens')
                    ->count();
        $surabayaPensCA = DB::table('kuisioner3')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'Pens')
                    ->count();
        $surabayaPensMerge = $surabayaPens + $surabayaPens2;
        
        $surabayaPpns = DB::table('kuisioner1')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'ppns')
                    ->count();
        $surabayaPpns2 = DB::table('kuisioner2')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'ppns')
                    ->count();
        $surabayaPpnsCA = DB::table('kuisioner3')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'ppns')
                    ->count();
        $surabayaPpnsMerge = $surabayaPpns + $surabayaPpns2;

        $surabayaPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'polinema')
                    ->count();
        $surabayaPolinema2 = DB::table('kuisioner2')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'polinema')
                    ->count();
        $surabayaPolinemaCA = DB::table('kuisioner3')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'polinema')
                    ->count();
        $surabayaPolinemaMerge = $surabayaPolinema + $surabayaPolinema2;

        $surabayaPolije = DB::table('kuisioner1')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'polije')
                    ->count();
        $surabayaPolije2 = DB::table('kuisioner2')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'Polije')
                    ->count();
        $surabayaPolijeCA = DB::table('kuisioner3')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'Polije')
                    ->count();
        $surabayaPolijeMerge = $surabayaPolije + $surabayaPolije2;

        $kediriPens = DB::table('kuisioner1')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'pens')
                    ->count();
        $kediriPens2 = DB::table('kuisioner2')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'pens')
                    ->count();
        $kediriPensCA = DB::table('kuisioner3')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'pens')
                    ->count();
        $kediriPensMerge = $kediriPens + $kediriPens2;

        $kediriPpns = DB::table('kuisioner1')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'ppns')
                    ->count();
        $kediriPpns2 = DB::table('kuisioner2')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'ppns')
                    ->count();
        $kediriPpnsCA = DB::table('kuisioner3')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'ppns')
                    ->count();
        $kediriPpnsMerge = $kediriPpns + $kediriPpns2;

        $kediriPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'polinema')
                    ->count();
        $kediriPolinema2 = DB::table('kuisioner2')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'polinema')
                    ->count();
        $kediriPolinemaCA = DB::table('kuisioner3')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'polinema')
                    ->count();
        $kediriPolinemaMerge = $kediriPolinema + $kediriPolinema2;

        $kediriPolije = DB::table('kuisioner1')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'polije')
                    ->count();
        $kediriPolije2 = DB::table('kuisioner2')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'polije')
                    ->count();
        $kediriPolijeCA = DB::table('kuisioner3')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'polije')
                    ->count();
        $kediriPolijeMerge = $kediriPolije + $kediriPolije2;

        $pamekasanPens = DB::table('kuisioner1')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'pens')
                    ->count();
        $pamekasanPens2 = DB::table('kuisioner2')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'pens')
                    ->count();
        $pamekasanPensCA = DB::table('kuisioner3')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'pens')
                    ->count();
        $pamekasanPensMerge = $pamekasanPens + $pamekasanPens2;

        $pamekasanPpns = DB::table('kuisioner1')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $pamekasanPpns2 = DB::table('kuisioner2')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $pamekasanPpnsCA = DB::table('kuisioner3')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $pamekasanPpnsMerge = $pamekasanPpns + $pamekasanPpns2;

        $pamekasanPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $pamekasanPolinema2 = DB::table('kuisioner2')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $pamekasanPolinemaCA = DB::table('kuisioner3')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $pamekasanPolinemaMerge = $pamekasanPolinema + $pamekasanPolinema2;

        $pamekasanPolije = DB::table('kuisioner1')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'polije')
                    ->count();
        $pamekasanPolije2 = DB::table('kuisioner2')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'polije')
                    ->count();
        $pamekasanPolijeCA = DB::table('kuisioner3')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'polije')
                    ->count();
        $pamekasanPolijeMerge = $pamekasanPolije + $pamekasanPolije2;

        $bangkalanPens = DB::table('kuisioner1')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'pens')
                    ->count();
        $bangkalanPens2 = DB::table('kuisioner2')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'pens')
                    ->count();
        $bangkalanPensCA = DB::table('kuisioner3')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'pens')
                    ->count();
        $bangkalanPensMerge = $bangkalanPens + $bangkalanPens2;

        $bangkalanPpns = DB::table('kuisioner1')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $bangkalanPpns2 = DB::table('kuisioner2')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $bangkalanPpnsCA = DB::table('kuisioner3')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $bangkalanPpnsMerge = $bangkalanPpns + $bangkalanPpns2;

        $bangkalanPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $bangkalanPolinema2 = DB::table('kuisioner2')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $bangkalanPolinemaCA = DB::table('kuisioner3')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $bangkalanPolinemaMerge = $bangkalanPolinema + $bangkalanPolinema2;

        $bangkalanPolije = DB::table('kuisioner1')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'polije')
                    ->count();
        $bangkalanPolije2 = DB::table('kuisioner2')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'polije')
                    ->count();
        $bangkalanPolijeCA = DB::table('kuisioner3')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'polije')
                    ->count();
        $bangkalanPolijeMerge = $bangkalanPolije + $bangkalanPolije2;
        
        $madiunPens = DB::table('kuisioner1')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'pens')
                    ->count();
        $madiunPens2 = DB::table('kuisioner2')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'pens')
                    ->count();
        $madiunPensCA = DB::table('kuisioner3')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'pens')
                    ->count();
        $madiunPensMerge = $madiunPens + $madiunPens2;

        $madiunPpns = DB::table('kuisioner1')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'ppns')
                    ->count();
        $madiunPpns2 = DB::table('kuisioner2')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'ppns')
                    ->count();
        $madiunPpnsCA = DB::table('kuisioner3')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'ppns')
                    ->count();
        $madiunPpnsMerge = $madiunPpns + $madiunPpns2;

        $madiunPolinema = DB::table('kuisioner1')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'polinema')
                    ->count();
        $madiunPolinema2 = DB::table('kuisioner2')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'polinema')
                    ->count();
        $madiunPolinemaCA = DB::table('kuisioner3')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'polinema')
                    ->count();
        $madiunPolinemaMerge = $madiunPolinema + $madiunPolinema2;

        $madiunPolije = DB::table('kuisioner1')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'polije')
                    ->count();
        $madiunPolije2 = DB::table('kuisioner2')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'polije')
                    ->count();
        $madiunPolijeCA = DB::table('kuisioner3')
                    ->where('kota', 'madiun')
                    ->where('politeknik', 'polije')
                    ->count();
        $madiunPolijeMerge = $madiunPolije + $madiunPolije2;

        $sidoarjoPens = DB::table('kuisioner1')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'pens')
                    ->count();
        $sidoarjoPens2 = DB::table('kuisioner2')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'pens')
                    ->count();
        $sidoarjoPensCA = DB::table('kuisioner3')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'pens')
                    ->count();
        $sidoarjoPensMerge = $sidoarjoPens + $sidoarjoPens2;

        $sidoarjoPpns = DB::table('kuisioner1')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'ppns')
                    ->count();
        $sidoarjoPpns2 = DB::table('kuisioner2')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'ppns')
                    ->count();
        $sidoarjoPpnsCA = DB::table('kuisioner3')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'ppns')
                    ->count();
        $sidoarjoPpnsMerge = $sidoarjoPpns + $sidoarjoPpns2;

        $sidoarjoPolinema = DB::table('kuisioner1')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'polinema')
                    ->count();
        $sidoarjoPolinema2 = DB::table('kuisioner2')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'polinema')
                    ->count();
        $sidoarjoPolinemaCA = DB::table('kuisioner3')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'polinema')
                    ->count();
        $sidoarjoPolinemaMerge = $sidoarjoPolinema + $sidoarjoPolinema2;

        $sidoarjoPolije = DB::table('kuisioner1')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'polije')
                    ->count();
        $sidoarjoPolije2 = DB::table('kuisioner2')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'polije')
                    ->count();
        $sidoarjoPolijeCA = DB::table('kuisioner3')
                    ->where('kota', 'sidoarjo')
                    ->where('politeknik', 'polije')
                    ->count();
        $sidoarjoPolijeMerge = $sidoarjoPolije + $sidoarjoPolije2;


        $compactData = array('lamonganMerge','sumenepMerge','surabayaMerge','kediriMerge','pamekasanMerge','bangkalanMerge'
                            ,'madiunMerge', 'sidoarjoMerge'
                            ,'lamonganPensMerge', 'lamonganPpnsMerge', 'lamonganPolinemaMerge', 'lamonganPolijeMerge'
                            ,'kediriPensMerge', 'kediriPpnsMerge', 'kediriPolinemaMerge', 'kediriPolijeMerge'
                            ,'sumenepPensMerge', 'sumenepPpnsMerge', 'sumenepPolinemaMerge', 'sumenepPolijeMerge'
                            ,'surabayaPensMerge', 'surabayaPpnsMerge', 'surabayaPolinemaMerge', 'surabayaPolijeMerge'
                            ,'pamekasanPensMerge', 'pamekasanPpnsMerge', 'pamekasanPolinemaMerge', 'pamekasanPolijeMerge'
                            ,'bangkalanPensMerge', 'bangkalanPpnsMerge', 'bangkalanPolinemaMerge', 'bangkalanPolijeMerge'
                            ,'madiunPensMerge', 'madiunPpnsMerge', 'madiunPolinemaMerge', 'madiunPolijeMerge'
                            ,'sidoarjoPensMerge', 'sidoarjoPpnsMerge', 'sidoarjoPolinemaMerge', 'sidoarjoPolijeMerge'

                            ,'lamonganPens','lamonganPpns','lamonganPolinema','lamonganPolije','sumenepPens','sumenepPpns','sumenepPolinema','sumenepPolije'
                            ,'surabayaPens','surabayaPpns','surabayaPolinema','surabayaPolije','kediriPens','kediriPpns','kediriPolinema','kediriPolije'
                            ,'pamekasanPens','pamekasanPpns','pamekasanPolinema','pamekasanPolije','bangkalanPens','bangkalanPpns','bangkalanPolinema','bangkalanPolije'
                            ,'kediriPens','kediriPpns','kediriPolinema','kediriPolije','sidoarjoPens','sidoarjoPpns','sidoarjoPolinema','sidoarjoPolije'

                            ,'lamonganPens2','lamonganPpns2','lamonganPolinema2','lamonganPolije2','sumenepPens2','sumenepPpns2','sumenepPolinema2','sumenepPolije2'
                            ,'surabayaPens2','surabayaPpns2','surabayaPolinema2','surabayaPolije2','kediriPens2','kediriPpns2','kediriPolinema2','kediriPolije2'
                            ,'pamekasanPens2','pamekasanPpns2','pamekasanPolinema2','pamekasanPolije2','bangkalanPens2','bangkalanPpns2','bangkalanPolinema2','bangkalanPolije2'
                            ,'madiunPens2','madiunPpns2','madiunPolinema2','madiunPolije2','sidoarjoPens2','sidoarjoPpns2','sidoarjoPolinema2','sidoarjoPolije2'

                            ,'lamonganPensCA','lamonganPpnsCA','lamonganPolinemaCA','lamonganPolijeCA','sumenepPensCA','sumenepPpnsCA','sumenepPolinemaCA','sumenepPolijeCA'
                            ,'surabayaPensCA','surabayaPpnsCA','surabayaPolinemaCA','surabayaPolijeCA','kediriPensCA','kediriPpnsCA','kediriPolinemaCA','kediriPolijeCA'
                            ,'pamekasanPensCA','pamekasanPpnsCA','pamekasanPolinemaCA','pamekasanPolijeCA','bangkalanPensCA','bangkalanPpnsCA','bangkalanPolinemaCA','bangkalanPolijeCA'
                            ,'madiunPensCA','madiunPpnsCA','madiunPolinemaCA','madiunPolijeCA','sidoarjoPensCA','sidoarjoPpnsCA','sidoarjoPolinemaCA','sidoarjoPolijeCA');
        return view('map.map', compact($compactData), [
            'db_kuisioner' => $db_kuisioner,
            'kuisioner' => $kuisioner
        ]);
    }
}