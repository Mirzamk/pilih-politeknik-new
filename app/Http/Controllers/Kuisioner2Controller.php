<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kuisioner2;
use DB;

class Kuisioner2Controller extends Controller
{
    public function index()
    {
        return view('kuisioner.kuisioner2');
    }

    public function store(Request $request)
    {
	DB::table('kuisioner2')->insert([
		'nama' => $request->nama,
		'nohp' => $request->nohp,
		'email' => $request->email,
		'sekolah' => $request->sekolah,
		'kota' => $request->kota,
		'kelas'=> $request->kelas,
        'politeknik' => $request->poltek,
		'politeknik2' => $request->poltek2,
		'bidang' => $request->bidang,
		'alasan' => $request->alasan,
		'informasi' => $request->informasi,
        'juara' => $request->juara,
	]);
	return redirect('/kuisioner2')->with('message', 'Data sudah ditambahkan, tunggu kabar selanjutnya');;
    }
}
