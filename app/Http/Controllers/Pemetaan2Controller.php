<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pemetaan2;
use DB;

class Pemetaan2Controller extends Controller
{
    public function index()
    {
        $lamongan = DB::table('kuisioner2')
                    ->where('kota', '=', 'lamongan')
                    ->count();
        $sumenep = DB::table('kuisioner2')
                    ->where('kota', 'arjasa')
                    ->orWhere('kota', 'sumenep')
                    ->count();
        $surabaya = DB::table('kuisioner2')
                    ->where('kota', 'surabaya')
                    ->count();
        $kediri = DB::table('kuisioner2')
                    ->where('kota', 'kediri')
                    ->orWhere('kota', 'papar')
                    ->orWhere('kota', 'kabupaten kediri')
                    ->count();
        $pamekasan = DB::table('kuisioner2')
                    ->where('kota', 'pamekasan')
                    ->count();
        $bangkalan = DB::table('kuisioner2')
                    ->where('kota', 'bangkalan')
                    ->count();
        $sidoarjo = DB::table('kuisioner2')
                    ->where('kota', 'sidoarjo')
                    ->count();
        $madiun = DB::table('kuisioner2')
                    ->where('kota', 'madiun')
                    ->orWhere('kota', 'kota madiun')
                    ->count();

        $smkn1lmgn = DB::table('kuisioner2')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->count();
        $smkn1arjasa = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->count();
        $smkn1ngasem = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->count();
        $smkn1brondong = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->count();
        $smkn1sby = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->count();
        $smkn3pamekasan = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->count();
        $sman1lamongan = DB::table('kuisioner2')
                    ->where('sekolah', 'sma negeri 1 lamongan')
                    ->orWhere('sekolah', 'sman 1 lamongan')
                    ->count();
        $sman15surabaya = DB::table('kuisioner2')
                    ->where('sekolah', 'sma negeri 15 surabaya')
                    ->orWhere('sekolah', 'sman 15 surabay')
                    ->orWhere('sekolah', 'sman 15 surabaya')
                    ->count();
        $sman4pamekasan = DB::table('kuisioner2')
                    ->where('sekolah', 'sma negeri 4 pamekasan')
                    ->orWhere('sekolah', 'sman 4 pamekasan')
                    ->count();
        $sman2pamekasan = DB::table('kuisioner2')
                    ->where('sekolah', 'sma negeri 2 pamekasan')
                    ->orWhere('sekolah', 'sman 2 pamekasan')
                    ->count();
        $sman1paciran = DB::table('kuisioner2')
                    ->where('sekolah', 'sma negeri 1 paciran')
                    ->orWhere('sekolah', 'sman 1 paciran')
                    ->count();
        $sman1sukodadi = DB::table('kuisioner2')
                    ->where('sekolah', 'sma negeri 1 sukodadi')
                    ->orWhere('sekolah', 'sman 1 sukodadi')
                    ->count();
        $sman2bangkalan = DB::table('kuisioner2')
                    ->where('sekolah', 'sma negeri 2 bangkalan')
                    ->orWhere('sekolah', 'sman 2 bangkalan')
                    ->count();
        $sman2madiun = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 2 MADIUN')
                    ->orWhere('sekolah', 'sman 02 madiun')
                    ->orWhere('sekolah', 'sma n 2 madiun')
                    ->count();
        $smamud4porong = DB::table('kuisioner2')
                    ->where('sekolah', 'SMA MUHAMMADIYAH 4 PORONG')
                    ->orWhere('sekolah', 'SMA MUHAMMADIYAH 4 PORONG SIDOARJO')
                    ->count();
        $smahangtuah5 = DB::table('kuisioner2')
                    ->where('sekolah', 'Sma Hangtuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hang tuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hangtuah 5')
                    ->orWhere('sekolah', 'Sma Hangtuah 5 candi')
                    ->orWhere('sekolah', 'Hangtuah 5')
                    ->count();
        $sman4sidoarjo = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 4 SIDOARJO')
                    ->count();

        $pens = DB::table('kuisioner2')
                    ->where('politeknik', 'pens')
                    ->count();
        $ppns = DB::table('kuisioner2')
                    ->where('politeknik', 'ppns')
                    ->count();
        $polinema = DB::table('kuisioner2')
                    ->where('politeknik', 'polinema')
                    ->count();
        $polije = DB::table('kuisioner2')
                    ->where('politeknik', 'polije')
                    ->count();

        $smkn1lmgnpens = DB::table('kuisioner2')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1lmgnppns = DB::table('kuisioner2')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1lmgnpolinema = DB::table('kuisioner2')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1lmgnpolije = DB::table('kuisioner2')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn1arjasapens = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1arjasappns = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1arjasapolinema = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1arjasapolije = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn1ngasempens = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1ngasemppns = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1ngasempolinema = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1ngasempolije = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn1brondongpens = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1brondongppns = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1brondongpolinema = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1brondongpolije = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn1sbypens = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1sbyppns = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1sbypolinema = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1sbypolije = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn3pamekasanpens = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn3pamekasanppns = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn3pamekasanpolinema = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn3pamekasanpolije = DB::table('kuisioner2')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->where('politeknik', 'polije')
                    ->count();
        $sman2madiunpens = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 2 MADIUN')
                    ->orWhere('sekolah', 'sman 02 madiun')
                    ->orWhere('sekolah', 'sma n 2 madiun')
                    ->where('politeknik','pens')
                    ->count();
        $sman2madiunppns = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 2 MADIUN')
                    ->orWhere('sekolah', 'sman 02 madiun')
                    ->orWhere('sekolah', 'sma n 2 madiun')
                    ->where('politeknik','ppns')
                    ->count();
        $sman2madiunpolinema = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 2 MADIUN')
                    ->orWhere('sekolah', 'sman 02 madiun')
                    ->orWhere('sekolah', 'sma n 2 madiun')
                    ->where('politeknik','polinema')
                    ->count();
        $sman2madiunpolije = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 2 MADIUN')
                    ->orWhere('sekolah', 'sman 02 madiun')
                    ->orWhere('sekolah', 'sma n 2 madiun')
                    ->where('politeknik','polije')
                    ->count();
        $smamud4porongpens = DB::table('kuisioner2')
                    ->where('sekolah', 'SMA MUHAMMADIYAH 4 PORONG')
                    ->orWhere('sekolah', 'SMA MUHAMMADIYAH 4 PORONG SIDOARJO')
                    ->where('politeknik','pens')
                    ->count();
        $smamud4porongppns = DB::table('kuisioner2')
                    ->where('sekolah', 'SMA MUHAMMADIYAH 4 PORONG')
                    ->orWhere('sekolah', 'SMA MUHAMMADIYAH 4 PORONG SIDOARJO')
                    ->where('politeknik','ppns')
                    ->count();
        $smamud4porongpolinema = DB::table('kuisioner2')
                    ->where('sekolah', 'SMA MUHAMMADIYAH 4 PORONG')
                    ->orWhere('sekolah', 'SMA MUHAMMADIYAH 4 PORONG SIDOARJO')
                    ->where('politeknik','polinema')
                    ->count();
        $smamud4porongpolije = DB::table('kuisioner2')
                    ->where('sekolah', 'SMA MUHAMMADIYAH 4 PORONG')
                    ->orWhere('sekolah', 'SMA MUHAMMADIYAH 4 PORONG SIDOARJO')
                    ->where('politeknik','polije')
                    ->count();
        $smahangtuah5pens = DB::table('kuisioner2')
                    ->where('sekolah', 'Sma Hangtuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hang tuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hangtuah 5')
                    ->orWhere('sekolah', 'Sma Hangtuah 5 candi')
                    ->orWhere('sekolah', 'Hangtuah 5')
                    ->where('politeknik','pens')
                    ->count();
        $smahangtuah5ppns = DB::table('kuisioner2')
                    ->where('sekolah', 'Sma Hangtuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hang tuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hangtuah 5')
                    ->orWhere('sekolah', 'Sma Hangtuah 5 candi')
                    ->orWhere('sekolah', 'Hangtuah 5')
                    ->where('politeknik','ppns')
                    ->count();
        $smahangtuah5polinema = DB::table('kuisioner2')
                    ->where('sekolah', 'Sma Hangtuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hang tuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hangtuah 5')
                    ->orWhere('sekolah', 'Sma Hangtuah 5 candi')
                    ->orWhere('sekolah', 'Hangtuah 5')
                    ->where('politeknik','polinema')
                    ->count();
        $smahangtuah5polije = DB::table('kuisioner2')
                    ->where('sekolah', 'Sma Hangtuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hang tuah 5 Sidoarjo')
                    ->orWhere('sekolah', 'Sma Hangtuah 5')
                    ->orWhere('sekolah', 'Sma Hangtuah 5 candi')
                    ->orWhere('sekolah', 'Hangtuah 5')
                    ->where('politeknik','polije')
                    ->count();
        $sman4sidoarjopens = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 4 SIDOARJO')
                    ->where('politeknik','pens')
                    ->count();
        $sman4sidoarjoppns = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 4 SIDOARJO')
                    ->where('politeknik','ppns')
                    ->count();
        $sman4sidoarjopolinema = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 4 SIDOARJO')
                    ->where('politeknik','polinema')
                    ->count();
        $sman4sidoarjopolije = DB::table('kuisioner2')
                    ->where('sekolah', 'SMAN 4 SIDOARJO')
                    ->where('politeknik','polije')
                    ->count();

        $compactData = array('lamongan','sumenep','surabaya','kediri','pamekasan','bangkalan','sidoarjo','madiun','smkn1lmgn','smkn1arjasa','smkn1ngasem','smkn1brondong','smkn1sby'
                            ,'smkn3pamekasan','sman15surabaya','sman1lamongan','sman4pamekasan','sman2pamekasan','sman1paciran','sman1sukodadi','sman4pamekasan','sman2bangkalan'
                            ,'pens','ppns','polinema','polije','smkn1lmgnpens','smkn1lmgnppns','smkn1lmgnpolinema','smkn1lmgnpolije','sman2madiun','smamud4porong','smahangtuah5','sman4sidoarjo'
                            ,'smkn1arjasapens','smkn1arjasappns','smkn1arjasapolinema','smkn1arjasapolije','smkn1ngasempens','smkn1ngasemppns','smkn1ngasempolinema','smkn1ngasempolije'
                            ,'smkn1brondongpens','smkn1brondongppns','smkn1brondongpolinema','smkn1brondongpolije','smkn1sbypens','smkn1sbyppns','smkn1sbypolinema','smkn1sbypolije'
                            ,'smkn3pamekasanpens','smkn3pamekasanppns','smkn3pamekasanpolinema','smkn3pamekasanpolije','sman2madiunpens','sman2madiunppns','sman2madiunpolinema','sman2madiunpolije'
                            ,'smamud4porongpens','smamud4porongppns','smamud4porongpolinema','smamud4porongpolije','smahangtuah5pens','smahangtuah5ppns','smahangtuah5polinema','smahangtuah5polije');
        return view('pemetaan.pemetaan2', compact($compactData));
    }
}
