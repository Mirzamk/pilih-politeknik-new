<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kuisioner1;
use DB;

class Kuisioner1Controller extends Controller
{
	public function index()
	{
		return view('kuisioner.kuisioner1');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'email' => 'unique:kuisioner1,email|email',
		]);

		DB::table('kuisioner1')->insert([
			'nama' => $request->nama,
			'nohp' => $request->nohp,
			'email' => $request->email,
			'sekolah' => $request->sekolah,
			'kota' => $request->kota,
			'kelas' => $request->kelas,
			'politeknik' => $request->poltek,
			'politeknik2' => $request->poltek2,
			'bidang' => $request->bidang,
			'alasan' => $request->alasan,
			'informasi' => $request->informasi,
		]);

		return redirect('/kuisioner1')->with('message', 'Data sudah ditambahkan, tunggu kabar selanjutnya');;
	}
}
