<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pemetaan;
use App\Models\Pemetaan2;
use DB;

class PemetaanController extends Controller
{
    public function index()
    {

        $lamongan = DB::table('kuisioner1')
                    ->where('kota', 'lamongan')
                    ->count();
        $lamongan2 = DB::table('kuisioner2')
                    ->where('kota', 'lamongan')
                    ->count();
        $lamonganMerge = $lamongan + $lamongan2;
        
        $sumenep = DB::table('kuisioner1')
                    ->orWhere('kota', 'sumenep')
                    ->count();
        $sumenep2 = DB::table('kuisioner2')
                    ->orWhere('kota', 'sumenep')
                    ->count();
        $sumenepMerge = $sumenep + $sumenep2;

        $surabaya = DB::table('kuisioner1')
                    ->where('kota', 'surabaya')
                    ->count();
        $surabaya2 = DB::table('kuisioner2')
                    ->where('kota', 'surabaya')
                    ->count();
        $surabayaMerge = $surabaya + $surabaya2;

        $kediri = DB::table('kuisioner1')
                    ->where('kota', 'kediri')
                    ->count();
        $kediri2 = DB::table('kuisioner2')
                    ->where('kota', 'kediri')
                    ->count();
        $kediriMerge = $kediri + $kediri2;

        $pamekasan = DB::table('kuisioner1')
                    ->where('kota', 'pamekasan')
                    ->count();
        $pamekasan2 = DB::table('kuisioner2')
                    ->where('kota', 'pamekasan')
                    ->count();
        $pamekasanMerge = $pamekasan + $pamekasan2;

        $bangkalan = DB::table('kuisioner1')
                    ->where('kota', 'bangkalan')
                    ->count();
        $bangkalan2 = DB::table('kuisioner2')
                    ->where('kota', 'bangkalan')
                    ->count();
        $bangkalanMerge = $bangkalan + $bangkalan2;


        $lamonganPens = DB::table('kuisioner1')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'pens')
                    ->count();
        $lamonganPens2 = DB::table('kuisioner2')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'pens')
                    ->count();
        $lamonganPensMerge = $lamonganPens + $lamonganPens2;

        $lamonganPpns = DB::table('kuisioner1')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $lamonganPpns2 = DB::table('kuisioner2')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'Ppns')
                    ->count();
        $lamonganPpnsMerge = $lamonganPpns + $lamonganPpns2;

        $lamonganPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $lamonganPolinema2 = DB::table('kuisioner2')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $lamonganPolinemaMerge = $lamonganPolinema + $lamonganPolinema2;

        $lamonganPolije = DB::table('kuisioner1')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polije')
                    ->count();
        $lamonganPolije2 = DB::table('kuisioner2')
                    ->where('kota', 'Lamongan')
                    ->where('politeknik', 'polije')
                    ->count();
        $lamonganPolijeMerge = $lamonganPolije + $lamonganPolije2;
        
        $sumenepPens = DB::table('kuisioner1')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'pens')
                    ->count();
        $sumenepPens2 = DB::table('kuisioner1')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'pens')
                    ->count();
        $sumenepPensMerge = $sumenepPens + $sumenepPens2;

        $sumenepPpns = DB::table('kuisioner1')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'ppns')
                    ->count();
        $sumenepPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'polinema')
                    ->count();
        $sumenepPolije = DB::table('kuisioner1')
                    ->where('kota', 'Sumenep')
                    ->where('politeknik', 'polije')
                    ->count();
        $surabayaPens = DB::table('kuisioner1')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'pens')
                    ->count();
        $surabayaPpns = DB::table('kuisioner1')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'ppns')
                    ->count();
        $surabayaPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'polinema')
                    ->count();
        $surabayaPolije = DB::table('kuisioner1')
                    ->where('kota', 'Surabaya')
                    ->where('politeknik', 'polije')
                    ->count();
        $kediriPens = DB::table('kuisioner1')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'pens')
                    ->count();
        $kediriPpns = DB::table('kuisioner1')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'ppns')
                    ->count();
        $kediriPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'polinema')
                    ->count();
        $kediriPolije = DB::table('kuisioner1')
                    ->where('kota', 'Kediri')
                    ->where('politeknik', 'polije')
                    ->count();
        $pamekasanPens = DB::table('kuisioner1')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'pens')
                    ->count();
        $pamekasanPpns = DB::table('kuisioner1')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $pamekasanPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $pamekasanPolije = DB::table('kuisioner1')
                    ->where('kota', 'Pamekasan')
                    ->where('politeknik', 'polije')
                    ->count();
        $bangkalanPens = DB::table('kuisioner1')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'pens')
                    ->count();
        $bangkalanPpns = DB::table('kuisioner1')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $bangkalanPolinema = DB::table('kuisioner1')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $bangkalanPolije = DB::table('kuisioner1')
                    ->where('kota', 'Bangkalan')
                    ->where('politeknik', 'polije')
                    ->count();

        $smkn1lmgn = DB::table('kuisioner1')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->count();
        $smkn1arjasa = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->count();
        $smkn1ngasem = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->count();
        $smkn1brondong = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->count();
        $smkn1sby = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->count();
        $smkn3pamekasan = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->count();
        $sman1lamongan = DB::table('kuisioner1')
                    ->where('sekolah', 'sma negeri 1 lamongan')
                    ->orWhere('sekolah', 'sman 1 lamongan')
                    ->count();
        $sman15surabaya = DB::table('kuisioner1')
                    ->where('sekolah', 'sma negeri 15 surabaya')
                    ->orWhere('sekolah', 'sman 15 surabay')
                    ->orWhere('sekolah', 'sman 15 surabaya')
                    ->count();
        $sman4pamekasan = DB::table('kuisioner1')
                    ->where('sekolah', 'sma negeri 4 pamekasan')
                    ->orWhere('sekolah', 'sman 4 pamekasan')
                    ->count();
        $sman2pamekasan = DB::table('kuisioner1')
                    ->where('sekolah', 'sma negeri 2 pamekasan')
                    ->orWhere('sekolah', 'sman 2 pamekasan')
                    ->count();
        $sman1paciran = DB::table('kuisioner1')
                    ->where('sekolah', 'sma negeri 1 paciran')
                    ->orWhere('sekolah', 'sman 1 paciran')
                    ->count();
        $sman1sukodadi = DB::table('kuisioner1')
                    ->where('sekolah', 'sma negeri 1 sukodadi')
                    ->orWhere('sekolah', 'sman 1 sukodadi')
                    ->count();
        $sman2bangkalan = DB::table('kuisioner1')
                    ->where('sekolah', 'sma negeri 2 bangkalan')
                    ->orWhere('sekolah', 'sman 2 bangkalan')
                    ->count();

        $pens = DB::table('kuisioner1')
                    ->where('politeknik', 'pens')
                    ->count();
        $ppns = DB::table('kuisioner1')
                    ->where('politeknik', 'ppns')
                    ->count();
        $polinema = DB::table('kuisioner1')
                    ->where('politeknik', 'polinema')
                    ->count();
        $polije = DB::table('kuisioner1')
                    ->where('politeknik', 'polije')
                    ->count();

        $smkn1lmgnpens = DB::table('kuisioner1')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1lmgnppns = DB::table('kuisioner1')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1lmgnpolinema = DB::table('kuisioner1')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1lmgnpolije = DB::table('kuisioner1')
                    ->where('sekolah', 'smkn 1 lamongan')
                    ->orWhere('sekolah', 'smk negeri 1 lamongan')
                    ->orWhere('sekolah', 'smkn 1 lmng')
                    ->orWhere('sekolah', 'smk n 1 lamongan')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn1arjasapens = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1arjasappns = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1arjasapolinema = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1arjasapolije = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 arjasa')
                    ->orWhere('sekolah', 'smkn 1 arjasa')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn1ngasempens = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1ngasemppns = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1ngasempolinema = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1ngasempolije = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 ngasem')
                    ->orWhere('sekolah', 'smkn 1 ngasem')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn1brondongpens = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1brondongppns = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1brondongpolinema = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1brondongpolije = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 brondong')
                    ->orWhere('sekolah', 'smkn 1 brondong')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn1sbypens = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn1sbyppns = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn1sbypolinema = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn1sbypolije = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 surabaya')
                    ->orWhere('sekolah', 'smkn 1 sby')
                    ->where('politeknik', 'polije')
                    ->count();
        $smkn3pamekasanpens = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->where('politeknik', 'pens')
                    ->count();
        $smkn3pamekasanppns = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->where('politeknik', 'ppns')
                    ->count();
        $smkn3pamekasanpolinema = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->where('politeknik', 'polinema')
                    ->count();
        $smkn3pamekasanpolije = DB::table('kuisioner1')
                    ->where('sekolah', 'smk negeri 3 pamekasan')
                    ->orWhere('sekolah', 'smkn 3 pamekasan')
                    ->where('politeknik', 'polije')
                    ->count();

        $compactData = array('lamongan','sumenep','surabaya','kediri','pamekasan','bangkalan','smkn1lmgn','smkn1arjasa','smkn1ngasem','smkn1brondong','smkn1sby'
                            ,'smkn3pamekasan','sman15surabaya','sman1lamongan','sman4pamekasan','sman2pamekasan','sman1paciran','sman1sukodadi','sman4pamekasan','sman2bangkalan'
                            ,'pens','ppns','polinema','polije','smkn1lmgnpens','smkn1lmgnppns','smkn1lmgnpolinema','smkn1lmgnpolije'
                            ,'smkn1arjasapens','smkn1arjasappns','smkn1arjasapolinema','smkn1arjasapolije','smkn1ngasempens','smkn1ngasemppns','smkn1ngasempolinema','smkn1ngasempolije'
                            ,'smkn1brondongpens','smkn1brondongppns','smkn1brondongpolinema','smkn1brondongpolije','smkn1sbypens','smkn1sbyppns','smkn1sbypolinema','smkn1sbypolije'
                            ,'lamonganPens','lamonganPpns','lamonganPolinema','lamonganPolije','sumenepPens','sumenepPpns','sumenepPolinema','sumenepPolije'
                            ,'surabayaPens','surabayaPpns','surabayaPolinema','surabayaPolije','kediriPens','kediriPpns','kediriPolinema','kediriPolije'
                            ,'pamekasanPens','pamekasanPpns','pamekasanPolinema','pamekasanPolije','bangkalanPens','bangkalanPpns','bangkalanPolinema','bangkalanPolije');
        return view('pemetaan.pemetaan', compact($compactData));
    }
}
