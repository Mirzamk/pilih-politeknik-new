<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemetaan extends Model
{
    protected $table = 'kuisioner1';

    use HasFactory;
    protected $primaryKey = 'id_kuisioner1';
}
