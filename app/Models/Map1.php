<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Map1 extends Model
{
    protected $table = 'kuisioner3';
    protected $primaryKey = 'id_kuisioner3';
    use HasFactory;   
}
