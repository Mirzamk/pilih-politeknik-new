<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kuisioner2 extends Model
{
    protected $table = 'kuisioner2';
    protected $fillable = [
        'nama',
		'email',
		'sekolah',
        'kota',
        'kelas',
        'politeknik',
        'politeknik2',
		'bidang', 
		'alasan', 
		'informasi',
        ];

    use HasFactory;
    protected $primaryKey = 'id_kuisioner2';
}
