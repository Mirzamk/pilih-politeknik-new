<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemetaan2 extends Model
{
    protected $table = 'kuisioner2';

    use HasFactory;
    protected $primaryKey = 'id_kuisioner2';
}
