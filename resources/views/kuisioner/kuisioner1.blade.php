<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pilih Politeknik</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Theme style -->
  <link rel="stylesheet" href="bootstrap/app.css">
</head>

<body class="hold-transition sidebar-mini">
  <style>
    .content {
      max-width: 100%;
      width: 800px;
      margin: auto;
    }

    p.question {
      font-family: Arial, sans-serif;
      font-size: 15px;
      color: #2E2E2E;
      margin-bottom: 0px;
    }

  </style>
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background-color:#FFFF00">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li>
          <a href="kuisioner1" style="color: black; font-size:20px">Kuisioner1</a>
          <a href="kuisioner2" style="color: black; font-size:20px">Kuisioner2</a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <!--{{-- <aside style="background-color:#0080ff" class="main-sidebar sidebar-dark-primary elevation-4">-->
    <!-- Brand Logo -->
  <!--  <a href="/" class="brand-link">-->
  <!--    <img src="adminlte/img/avatar.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">-->
  <!--    <span class="brand-text font-weight-light">Web</span>-->
  <!--  </a>-->
    <!-- /.sidebar -->
  <!--</aside> --}}-->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content">
        <div class="col-md-12 content">
          <div class="col-md-12 content">
            <h1 style="text-align: center">Kuisioner Peminatan Siswa Terhadap Politeknik</h1><br>
            <label style="text-align: justify; font-size:18px">Kuisioner ini untuk siswa SMA/SMK/MA kelas 10, 11 dan 12.
              Kami bertujuan untuk mendapatkan minat
              siswa untuk melanjutkan pendidikan ke Politeknik, sehingga ada keberlanjutan pembelajaran yang
              tepat.</label>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="well col-md-12">
          @if ($errors->any())
            <div class="alert alert-danger">
              @foreach ($errors->all() as $error)
                <h4 style="color:#833333; font-weight: bold"> {{ $error }} </h4>
              @endforeach
            </div>
          @endif
          <h4 style="color:#338333; font-weight: bold"> {{ Session::get('message') }} </h4>
          <h3>Gambar - gambar di bawah ini merupakan logo dari politeknik di Jawa Timur, pilihlah jawaban di bawah ini
            yang sesuai </h3>
          <table>
            <div>
              <tr>
                <td>
                  <label class="question">1. <img src="img/Logo_Polinema.jpg" alt="Logo POLINEMA"
                      class="img-responsive" width="150" style="margin-left: 20px"><br> </label>
                </td>
                <td>
                  <ul>
                    <input class="answer" type="radio" name="q1" value="1">
                    <label id="correctString1">POLINEMA</label>
                    <br>
                    <input class="answer" type="radio" name="q1" value="0">
                    <label>PENS</label>
                    <br>
                    <input class="answer" type="radio" name="q1" value="0">
                    <label>PPNS</label>
                    <br>
                    <input class="answer" type="radio" name="q1" value="0">
                    <label>POLIJE</label>
                  </ul>
                </td>
              </tr>
            </div>
            <div>
              <tr>
                <td>
                  <label class="question">2. <img src="img/Logo_PPNS.jpg" alt="Logo PPNS" class="img-responsive"
                      width="150" style="margin-left: 20px"><br> </label>
                </td>
                <td>
                  <ul>
                    <input class="answer" type="radio" name="q2" value="0">
                    <label>POLINEMA</label>
                    <br>
                    <input class="answer" type="radio" name="q2" value="0">
                    <label>PENS</label>
                    <br>
                    <input class="answer" type="radio" name="q2" value="1">
                    <label id="correctString2">PPNS</label>
                    <br>
                    <input class="answer" type="radio" name="q2" value="0">
                    <label>POLIJE</label>
                  </ul>
                </td>
              </tr>
            </div>

            <div>
              <tr>
                <td>
                  <label class="question">3. <img src="img/Logo_PENS.jpg" alt="Logo PENS" class="img-responsive"
                      height="150" style="margin-left: 20px"><br> </label>
                </td>
                <td>
                  <ul>
                    <input class="answer" type="radio" name="q3" value="0">
                    <label>POLINEMA</label>
                    <br>
                    <input class="answer" type="radio" name="q3" value="1">
                    <label id="correctString3">PENS</label>
                    <br>
                    <input class="answer" type="radio" name="q3" value="0">
                    <label>PPNS</label>
                    <br>
                    <input class="answer" type="radio" name="q3" value="0">
                    <label>POLIJE</label>
                  </ul>
                </td>
              </tr>
            </div>
            <div>
              <tr>
                <td>
                  <label class="question">4. <img src="img/Logo_Polije.jpg" alt="Logo POLIJE"
                      class="img-responsive" width="150" style="margin-left: 20px"><br></label>
                </td>
                <td>
                  <ul>
                    <input class="answer" type="radio" name="q4" value="0">
                    <label>POLINEMA</label>
                    <br>
                    <input class="answer" type="radio" name="q4" value="0">
                    <label>PENS</label>
                    <br>
                    <input class="answer" type="radio" name="q4" value="0">
                    <label>PPNS</label>
                    <br>
                    <input class="answer" type="radio" name="q4" value="1">
                    <label id="correctString4">POLIJE</label>
                  </ul>
                </td>
              </tr>
            </div>
          </table>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <form class="form-horizontal" action="{{ url('/kuisioner1') }}" method="post">
            {{ csrf_field() }}
            <fieldset>
              <h3>Isikan Identitas Kamu Terkait Politeknik Yang Ingin Kamu Pilih Untuk Mengenal Kamu Lebih Baik</h3>
              <label for="wajib" style="color:red">* Wajib</label>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <div class="form-group">
            <br><label for="nama" class="col-lg-12">Nama kamu siapa? <label for="wajib"
                style="color:red">*</label></label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama kamu ..." required>
            </div>
          </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <div class="form-group">
            <label for="noHP" class="col-lg-12">Nomer HP kamu berapa?</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="nohp" name="nohp" placeholder="Nomer HP kamu ...">
            </div>
          </div>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <div class="form-group">
            <label for="email" class="col-lg-12">Email kamu apa? <label for="wajib"
                style="color:red">*</label></label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="email" name="email" placeholder="Email kamu ..." required>
            </div>
          </div>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-5">
                <label for="sekolah">Kamu bersekolah dimana? <label for="wajib" style="color:red">*</label></label>
                <input type="text" class="form-control" id="sekolah" name="sekolah" placeholder="Sekolah kamu ..."
                  required>
              </div>
              <div class="col-md-5">
                <label for="kota">Dari kota apa? <label for="wajib" style="color:red">*</label></label>
                @php
                  $daftarKota = ['Lamongan', 'Surabaya', 'Gresik'];
                @endphp
                <select class="form-control" name="kota" id="kota">
                  @foreach ($daftarKota as $kota)
                    <option value="{{ $kota }}">{{ $kota }}</option>
                  @endforeach
                </select>
                {{-- <input type="text" class="form-control" id="kota" name="kota" placeholder="Kota sekolah kamu ..."
                  required> --}}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <div class="form-group">
            <label for="kelas" class="col-lg-12">Kamu kelas berapa? <label for="wajib"
                style="color:red">*</label></label>
            <div class="col-lg-12">
              <input type="radio" id="10" name="kelas" value="10">
              <label for="10">10</label>
              <p></p>
              <input type="radio" id="11" name="kelas" value="11">
              <label for="11">11</label>
              <p></p>
              <input type="radio" id="12" name="kelas" value="12">
              <label for="12">12</label>
              <p></p>
            </div>
          </div>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <h3 class="card-title">Pilihlah Pilihan Terbaikmu Terhadap Politeknik di Bawah Ini : </h3>
          <div class="form-group">
            <label for="kelas" class="col-lg-12">Pilihan Pertamamu Memilih Politeknik Apa? <label for="wajib"
                style="color:red">*</label></label>
            <div class="col-lg-12">
              <input type="radio" id="pens" name="poltek" value="pens">
              <label for="pens">PENS </label><br>
              <input type="radio" id="ppns" name="poltek" value="ppns">
              <label for="ppns">PPNS</label><br>
              <input type="radio" id="polinema" name="poltek" value="polinema">
              <label for="polinema">POLINEMA</label><br>
              <input type="radio" id="polije" name="poltek" value="polije">
              <label for="polije">POLIJE</label><br><br>
            </div>
          </div>
          <div class="form-group">
            <label for="kelas" class="col-lg-12">Pilihan Keduamu Memilih Politeknik Apa? <label for="wajib"
                style="color:red">*</label></label>
            <div class="col-lg-12">
              <input type="radio" id="pens2" name="poltek2" value="pens">
              <label for="pens2">PENS </label><br>
              <input type="radio" id="ppns2" name="poltek2" value="ppns">
              <label for="ppns2">PPNS </label><br>
              <input type="radio" id="polinema2" name="poltek2" value="polinema">
              <label for="polinema2">POLINEMA </label><br>
              <input type="radio" id="polije2" name="poltek2" value="polije">
              <label for="polije2">POLIJE </label><br><br>
            </div>
          </div>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <div class="form-group">
            <label for="programStudi" class="col-lg-12">Program Studi Apa Yang Ingin Kamu Pilih? <label for="wajib"
                style="color:red">*</label></label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="bidang" name="bidang" placeholder="Program studi ..."
                required>
            </div>
          </div>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <div class="form-group ">
            <label for="textArea" class="col-lg-12">Apa Alasanmu Memilih Politeknik Diatas? <label for="wajib"
                style="color:red">*</label></label>
            <div class="col-lg-5">
              <textarea class="form-control" rows="3" id="alasan" name="alasan" required></textarea>
            </div>
          </div>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
          <div class="form-group ">
            <label for="textArea" class="col-lg-12">Darimana Kamu Mengetahui Politeknik Yang Kamu Pilih? <label
                for="wajib" style="color:red">*</label></label>
            <div class="col-lg-5">
              <textarea class="form-control" rows="3" id="informasi" name="informasi" required></textarea>
            </div>
          </div>
          <div class="checkbox col-md-12">
            <label>
              <input type="checkbox" id="sedia" name="sedia" value="sedia" required>Apakah kamu bersedia untuk dikontak
              melalui E-Mail selanjutnya ?
            </label>
          </div>
          <div class="form-group">
            <div class="col-lg-10 col-lg-offset-0">
              <input type="submit" class="btn btn-primary">
            </div>
          </div>
        </div>
      </section>
      </fieldset>
      <!-- /.card -->
      </form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

</body>
<script>
  function submitQuiz() {
    console.log('submitted');

    // get each answer score
    function answerScore(qName) {
      var radiosNo = document.getElementsByName(qName);

      for (var i = 0, length = radiosNo.length; i < length; i++) {
        if (radiosNo[i].checked) {
          // do something with radiosNo
          var answerValue = Number(radiosNo[i].value);
        }
      }
      // change NaNs to zero
      if (isNaN(answerValue)) {
        answerValue = 0;
      }
      return answerValue;
    }

    // calc score with answerScore function
    var calcScore = (answerScore('q1') + answerScore('q2') + answerScore('q3') + answerScore('q4'));
    console.log("CalcScore: " + calcScore); // it works!

    // function to return correct answer string
    function correctAnswer(correctStringNo, qNumber) {
      console.log("qNumber: " + qNumber); // logs 1,2,3,4 after called below
      return ("Jawaban yang benar dari pertanyaan nomer" + qNumber + ": &nbsp;<strong>" +
        (document.getElementById(correctStringNo).innerHTML) + "</strong>");
    }

    // print correct answers only if wrong (calls correctAnswer function)
    if (answerScore('q1') === 0) {
      document.getElementById('correctAnswer1').innerHTML = correctAnswer('correctString1', 1);
    }
    if (answerScore('q2') === 0) {
      document.getElementById('correctAnswer2').innerHTML = correctAnswer('correctString2', 2);
    }
    if (answerScore('q3') === 0) {
      document.getElementById('correctAnswer3').innerHTML = correctAnswer('correctString3', 3);
    }
    if (answerScore('q4') === 0) {
      document.getElementById('correctAnswer4').innerHTML = correctAnswer('correctString4', 4);
    }

  };
</script>

</html>
