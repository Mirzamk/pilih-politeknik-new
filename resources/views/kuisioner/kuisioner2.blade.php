<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pilih Politeknik</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Theme style -->
  <link rel="stylesheet" href="bootstrap/app.css">
</head>
<body class="hold-transition sidebar-mini">
    <style>
      .content {max-width: 100%; width: 800px; margin: auto;}
      p.question {font-family: Arial, sans-serif;font-size:15px;color: #2E2E2E;margin-bottom:0px;}
  </style>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li>
        <a href="kuisioner1" style="color: black; font-size:20px">Kuisioner1</a>
        <a href="kuisioner2" style="color: black; font-size:20px">Kuisioner2</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <!--{{-- <aside style="background-color:#0080ff" class="main-sidebar sidebar-dark-primary elevation-4">-->
    <!-- Brand Logo -->
  <!--  <a href="/" class="brand-link">-->
  <!--    <img src="adminlte/img/avatar.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">-->
  <!--    <span class="brand-text font-weight-light">Web</span>-->
  <!--  </a>-->
    <!-- /.sidebar -->
  <!--</aside> --}}-->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content">
        <div class="col-md-12 content">
          <div class="col-md-12 content">
            <h1  style="text-align: center">Kuisioner Peminatan Siswa Terhadap Politeknik</h1><br>
            <label style="text-align: justify; font-size:18px">Kuisioner ini untuk siswa SMA/SMK/MA kelas 10, 11 dan 12. Kami bertujuan untuk mendapatkan minat 
              siswa untuk melanjutkan pendidikan ke Politeknik, sehingga ada keberlanjutan pembelajaran yang tepat.</label>
              <h3 style="color: black;">Berikut merupakan prestasi lomba dari 2 Politeknik besar yang ada di Jawa Timur. </h3>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="well col-md-12">
        <h4 style="color:#338333; font-weight: bold"> {{ Session::get('message') }} </h4>
      <div>
        <h4>Angkat Topik Perubahan Iklim, Mahasiswa PENS Berhasil Torehkan Prestasi pada Science Video Challenge</h4>
        <img src="img/berita_pens_1.jpg" alt="berita_pens_1" class="img-responsive" width="740" height="640">
        <br>
        <p style="text-align: justify">
            Surabaya, pens.ac.id – Sebuah prestasi membanggakan kembali diraih oleh mahasiswa Politeknik Elektronika Negeri Surabaya (PENS).KakaTeam yang beranggotakan 
             tiga mahasiswa dari Program Studi Teknologi Multimedia Broadcasting yakni Adella Delvina, Mahdanu Oktarinda Fajria Sukma, dan Gallan Romansyah 
             berhasil menyabet Juara Pertama pada ajang Science Video Challenge (SVC) yang dihelat oleh KokBisa, Madani Berkelanjutan, dan Golongan Hutan. 
             Pengumuman pemenang perlombaan dilaksanakan pada Kamis (22/4) secara online melalui media sosial akun instagram KokBisa.
        </p>
        <p style="text-align: justify">
            Perjalanan para anggota KakaTeam untuk menjadi juara memang tidak mudah. Berbagai tahapan yang panjang telah dilalui, dimulai dari tahap pendaftaran, babak penyisihan, hingga terdapat 
            kelas mentoring pembuatan video animasi oleh tim KokBisa sejak (27/2-22/4). Dari proses panjang tersebut, akhirnya tiga mahasiswa Program Studi Teknologi Multimedia Brodcasting 
            ini berhasil membuat karya dengan judul “Apakah Perubahan Iklim Dapat Membuat Negara Jatuh Miskin?” yang dapat dilihat melalui channel YouTube resmi KokBisa. Video tersebut 
            mengangkat tema tentang dampak ekonomi dari perubahan iklim, yang mana tidak hanya membuka wawasan baru, tetapi juga mengajak para penonton untuk menjaga dan melestarikan lingkungan.
        </p>
        <p style="text-align: justify">
            KakaTeam telah terbentuk sejak 2020 dan memiliki tiga orang anggota tim yang hebat di bidangnya. Meskipun mereka bertiga merupakan mahasiswa tingkat akhir namun tidak menjadi hambatan 
            mereka untuk berkarya dan berprestasi. “Riset materi yang dilakukan jauh di luar bidang studi kami, kami berusaha semaksimal mungkin dan fokus pada riset serta penyusunan script agar 
            alur video kami bisa terbentuk dengan baik,” ujar Gallan Romansyah.
        </p>
        <p style="text-align: justify">
            Pada hari pengumuman tiba, para anggota KakaTeam merasa terkejut dan senang karena telah mengalahkan 30 tim yang mengikuti perlombaan tersebut. Mereka berharap agar bisa terus mengasah 
            skill mereka dibidang masing-masing supaya terus berkarya. “Kami juga berharap ini bisa menjadi batu loncatan untuk karir kami di bidang animasi dan lainnya,” harap Gallan. (jes/raf)
        </p>
        <label for="source"><a href="https://www.pens.ac.id/2021/03/23/dinobatkan-juara-1-kategori-robot-transporter-tim-sieera-photovoltaic-berhasil-mengalahkan-35-tim-pesaing/">
            https://www.pens.ac.id/2021/03/23/dinobatkan-juara-1-kategori-robot-transporter-tim-sieera-photovoltaic-berhasil-mengalahkan-35-tim-pesaing/</a> </label>
      </div>
    </section>
    <section class="content">
      <div class="well col-md-12">
        <h4>Angkat Topik Perubahan Iklim, Mahasiswa PENS Berhasil Torehkan Prestasi pada Science Video Challenge</h4>
        <img src="img/berita_pens_2.jpg" alt="berita_pens_2" class="img-responsive" width="740" height="640">
        <br>
        <p style="text-align: justify">Surabaya, pens.ac.id – Prestasi membanggakan kembali diraih oleh mahasiswa Politeknik Elektronika Negeri Surabaya
             (PENS). Tiga mahasiswa Program Studi Teknik Elektronika dan Teknik Mekatronika yang tergabung dalam Tim Sieera Photovoltaic berhasil menyabet 
             predikat Juara 1 mengalahkan 35 tim lainnya pada kategori robot transporter tingkat nasional. Diikuti selama dua hari (20-21/3), Electrical and 
             Computer Competition (ELCCO) ini dihelat oleh Universitas Udayana secara online melalui Webex Video Meetings.
        </p>
        <p style="text-align: justify">Tim Sieera Photovoltaic beranggotakan Dewa Pramudya dan Asad Fakih yang merupakan mahasiswa Program Studi Teknik Elektronika 
             serta Putri Gita Artha Adjani Syindy dari Program Studi Teknik Mekatronika. Berawal dari pengalaman mengikuti kompetisi sejenis, Dewa dan tim dengan sergap
             mempersiapkan robot untuk kompetisi serta komponen-komponen yang dibutuhkan. Tidak hanya itu timnya juga mempersiapkan jalur yang telah disediakan panitia sebagai 
             bahan untuk berlatih agar bisa mendapatkan hasil yang maksimal.
        </p>
        <p style="text-align: justify">
            Persaingan yang begitu ketat cukup terasa pada saat kompetisi yang menuntut ketepatan, kecepatan, dan juga desain robot. Namun, Tim Sieera Photovoltaic berhasil
             menghadapi pesaing-pesaing yang sudah berpengalaman. “Pesaing lomba kali ini cukup berat dan sangat berpengalaman, namun kami terus berusaha dan mencoba semaksimal 
             mungkin serta turut menghadirkan keyakinan kuat terhadap tim yang telah kita bentuk,” ungkap Dewa.
        </p>
        <p style="text-align: justify">
            Kemenangan yang ditorehkan kali ini, tidak lepas dari dukungan dari pihak PENS. Sarana yang diberikan serta perizinan dari kampus yang cukup mudah merupakan langkah awal
             dari kemenangan. Dewa dan tim sangat bersyukur dan tidak pernah terbayang sebelumnya bisa mendapatkan predikat juara kali ini. “Belum pernah kebayang sama sekali, pas waktu 
             final bener-bener deg-degan dan Alhamdulillah bisa menang. Senang dan bersyukur banget dengan kemenangan ini,” pungkas Dewa Pramudya. (irf/raf)
        </p>
        <label for="source"><a href="https://www.pens.ac.id/2021/05/02/angkat-topik-perubahan-iklim-mahasiswa-pens-berhasil-torehkan-prestasi-pada-science-video-challenge/">
            https://www.pens.ac.id/2021/05/02/angkat-topik-perubahan-iklim-mahasiswa-pens-berhasil-torehkan-prestasi-pada-science-video-challenge/</a> </label>
      </div>
    </section>
    <section class="content">
        <div class="well col-md-12">
          <h4>POLINEMA memperoleh Juara 1 pada ajang National Bridge Challenge 2020</h4>
          <img src="img/berita_polinema_1.jpg" alt="berita_polinema_1" class="img-responsive" width="740" height="640">
          <br>
          <p style="text-align: justify">Tim "Arya Satya" Jurusan Teknik Sipil Polinema berhasil meraih Juara I pada lomba 
            National Balsa Bridge Challenge 2020 yang diselenggarakan oleh Keluarga Mahasiswa Teknik Sipil - Universitas Muhammadiyah Surakarta. 
          </p>
          <p style="text-align: justify">Tim yang terdiri dari mahasiswa semester 3 Prodi D-IV Manajemen Rekayasa Konstruksi, 
            Muhammad Aly Kamil dan Khusnul Aldi Saputra, dibimbing oleh Drs. Armin Naibaho, ST., MT.⁣

            Congrats!!!
          </p>
          <label for="source"><a href="https://www.instagram.com/p/CIsbTJClyIg/?utm_medium=copy_link">
            https://www.instagram.com/p/CIsbTJClyIg/?utm_medium=copy_link</a> </label>
        </div>
      </section>
    <section class="content">
      <div class="well col-md-12">
      <form class="form-horizontal" action="{{ url('/kuisioner2') }}" method="post">
      {{ csrf_field() }}
      <fieldset>
        <h3>Isikan Identitas Kamu Terkait Politeknik Yang Ingin Kamu Pilih Untuk Mengenal Kamu Lebih Baik</h3>
          <label for="wajib" style="color:red">* Wajib</label>
          </div>
          </section>
        <section class="content">
        <div class="well col-md-12">
        <div class="form-group">
          <br><label for="nama" class="col-lg-12">Nama kamu siapa? <label for="wajib" style="color:red">*</label></label>
          <div class="col-lg-5">
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama kamu ..." required>
          </div>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
        <div class="form-group">
            <label for="noHP" class="col-lg-12">Nomer HP kamu berapa?</label>
          <div class="col-lg-5">
            <input type="text" class="form-control" id="nohp" name="nohp" placeholder="Nomer HP kamu ...">
          </div>
        </div>
        </div>
      </section>
        <section class="content">
          <div class="well col-md-12">
        <div class="form-group">
          <label for="email" class="col-lg-12">Email kamu apa? <label for="wajib" style="color:red">*</label></label>
          <div class="col-lg-5">
            <input type="text" class="form-control" id="email" name="email" placeholder="Email kamu ..." required>
          </div>
        </div>
          </div>
        </section>
        <section class="content">
        <div class="well col-md-12">
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-5">
              <label for="sekolah">Kamu bersekolah dimana? <label for="wajib" style="color:red">*</label></label>
              <input type="text" class="form-control" id="sekolah" name="sekolah" placeholder="Sekolah kamu ..." required>
            </div>
            <div class="col-md-5">
              <label for="kota">Dari kota apa? <label for="wajib" style="color:red">*</label></label>
              <input type="text" class="form-control" id="kota" name="kota" placeholder="Kota sekolah kamu ..." required>
            </div>
          </div>
        </div>
        </div>
        </section>
        <section class="content">
        <div class="well col-md-12">
        <div class="form-group">
          <label for="kelas" class="col-lg-12">Kamu kelas berapa? <label for="wajib" style="color:red">*</label></label>
          <div class="col-lg-12">
            <input type="radio" id="10" name="kelas" value="10">
            <label for="10">10</label><p></p>
            <input type="radio" id="11" name="kelas" value="11">
            <label for="11">11</label><p></p>
            <input type="radio" id="12" name="kelas" value="12">
            <label for="12">12</label><p></p>
          </div>
        </div>
        </div>
        </section>
        <section class="content">
        <div class="well col-md-12">
        <h3 class="card-title">Pilihlah Pilihan Terbaikmu Terhadap Politeknik di Bawah Ini : </h3>
        <div class="form-group">
          <label for="kelas" class="col-lg-12">Pilihan Pertamamu Memilih Politeknik Apa? <label for="wajib" style="color:red">*</label></label>
          <div class="col-lg-12">
            <input type="radio" id="pens" name="poltek" value="pens">
            <label for="pens">PENS </label><br>
            <input type="radio" id="ppns" name="poltek" value="ppns">
            <label for="ppns">PPNS</label><br>
            <input type="radio" id="polinema" name="poltek" value="polinema">
            <label for="polinema">POLINEMA</label><br>
            <input type="radio" id="polije" name="poltek" value="polije">
            <label for="polije">POLIJE</label><br><br>
          </div>
        </div>
        <div class="form-group">
          <label for="kelas" class="col-lg-12">Pilihan Keduamu Memilih Politeknik Apa? <label for="wajib" style="color:red">*</label></label>
          <div class="col-lg-12">
            <input type="radio" id="pens2" name="poltek2" value="pens">
            <label for="pens2">PENS </label><br>
            <input type="radio" id="ppns2" name="poltek2" value="ppns">
            <label for="ppns2">PPNS </label><br>
            <input type="radio" id="polinema2" name="poltek2" value="polinema">
            <label for="polinema2">POLINEMA </label><br>
            <input type="radio" id="polije2" name="poltek2" value="polije">
            <label for="polije2">POLIJE </label><br><br>
          </div>
        </div>
        </div>
        </section>
        <section class="content">
        <div class="well col-md-12">
        <div class="form-group">
          <label for="programStudi" class="col-lg-12">Program Studi Apa Yang Ingin Kamu Pilih? <label for="wajib" style="color:red">*</label></label>
          <div class="col-lg-5">
            <input type="text" class="form-control" id="bidang" name="bidang" placeholder="Program studi ..." required>
          </div>
        </div>
        </div>
        </section>
        <section class="content">
        <div class="well col-md-12">
        <div class="form-group ">
          <label for="textArea" class="col-lg-12" >Apa Alasanmu Memilih Politeknik Diatas? <label for="wajib" style="color:red">*</label></label>
          <div class="col-lg-5">
            <textarea class="form-control" rows="3" id="alasan" name="alasan" required></textarea>
          </div>
        </div>
        </div>
        </section>
        <section class="content">
        <div class="well col-md-12">
        <div class="form-group ">
          <label for="textArea" class="col-lg-12" >Darimana Kamu Mengetahui Politeknik Yang Kamu Pilih? <label for="wajib" style="color:red">*</label></label>
          <div class="col-lg-5">
            <textarea class="form-control" rows="3" id="informasi" name="informasi" required></textarea>
          </div>
        </div>
        </div>
      </section>
      <section class="content">
        <div class="well col-md-12">
        <div class="form-group">
          <label for="kelas" class="col-lg-12">Jika Anda masuk ke salah satu politeknik yang dipilih. Apakah anda tertarik untuk menjadi juara seperti berita di atas? <label for="wajib" style="color:red">*</label></label>
          <div class="col-lg-12">
            <input type="radio" id="ya" name="juara" value="ya">
            <label for="ya">YA </label><br>
            <input type="radio" id="tidak" name="juara" value="tidak">
            <label for="tidak">TIDAK </label><br>
          </div>
        </div>
        </div>
        <div class="checkbox col-md-12">
          <label>
            <input type="checkbox" id="sedia" name="sedia" value="sedia" required>Apakah kamu bersedia untuk dikontak 
            melalui E-Mail selanjutnya ?
          </label>
        </div>
        
        <div class="form-group">
          <div class="col-lg-10 col-lg-offset-0">
            <input type="submit" class="btn btn-primary">
          </div>
        </div>
        </div>
      </section>
      </fieldset>
      <!-- /.card -->
    </form>
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

</body>
</html>
