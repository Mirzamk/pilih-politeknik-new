<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pilih Politeknik</title>

  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="bootstrap/app.css">

  {{-- <link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" /> --}}

  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
    crossorigin="" />
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
    crossorigin=""></script>


  <style>
    html,
    body {
      height: 100%;
      margin: 0;
    }

    #map {
      width: 600px;
      height: 400px;
    }

  </style>

  <style>
    table, th, td {
    border-collapse: collapse;
    }
    #map {
      width: 1200px;
      height: 500px;
    }

    .info {
      padding: 6px 8px;
      font: 14px/16px Arial, Helvetica, sans-serif;
      background: white;
      background: rgba(255, 255, 255, 0.8);
      box-shadow: 0 0 15px rgba(0, 0, 0, 0.5);
      border-radius: 5px;
    }

    .info h4 {
      margin: 0 0 5px;
      color: #777;
    }

    .legend {
      text-align: left;
      line-height: 18px;
      color: #555;
    }

    .legend i {
      width: 18px;
      height: 18px;
      float: left;
      margin-right: 8px;
      opacity: 0.7;
    }

  </style>

</head>

<body class="hold-transition sidebar-mini">
  <style>
    .content {
      max-width: 100%;
      width: 1200px;
      margin: auto;
    }

    .form-select {
      height: 44px;
      border-radius: 8px;
    }

    .form-button {
      height: 44px;
      background: blue;
      color: white;
      padding-left: 1em;
      padding-right: 1em;
      border-radius: 8px;
    }

  </style>
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background-color:#FFFF00">
      <ul class="navbar-nav">
      </ul>
    </nav>

    <div class="content-wrapper">
      <section class="content">
        <div class="col-md-12 content">
          <div class="col-md-12 content">
            <h1 style="text-align: center">Tampilan Peta</h1><br>
          </div>
        </div>
      </section>

      <section class="content">
        <form action="{{ route('map') }}">
          <select name="kuisioner" class="form-select" name="" id="">
            <option {{ $kuisioner == 'kuisioner1' ? 'selected' : '' }} value="1">Kuisioner 1</option>
            <option {{ $kuisioner == 'kuisioner2' ? 'selected' : '' }} value="2">Kuisioner 2</option>
            <option {{ $kuisioner == 'kuisioner3' ? 'selected' : '' }} value="3">Cellular Automata</option>
          </select>
          <button class="form-button" type="submit">Konfirmasi</button>
        </form>
        <div id="map"></div>
        <link rel="stylesheet" href="{{ asset('js/map/leaflet.css') }}">
        <script src="{{ asset('js/map/leaflet.js') }}"></script>
        {{-- <script type="text/javascript" src="{!! asset('assets/js/jatim_kota.js') !!}"></script> --}}
        <script src="{{ asset('js/jatim_kota.js') }}"></script>
        <script type="text/javascript">
          var dbKuisioner = @json($db_kuisioner);
          var map = L.map('map').setView([-7.536064, 113.238402], 8);

          L.tileLayer(
            'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
              maxZoom: 18,
              attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
              id: 'mapbox/light-v9',
              tileSize: 512,
              zoomOffset: -1
            }).addTo(map);


          // control that shows state info on hover
          var info = L.control();

          info.onAdd = function(map) {
            this._div = L.DomUtil.create('div', 'info');
            this.update();
            return this._div;
          };

          info.update = function(props) {
            this._div.innerHTML = '<h4>Pilih Politeknik</h4>' + (props ?
              '<b>' + props.TYPE_2 + " " + props.NAME_2 + '</b><br />' +
              'Jumlah Kuisioner <br>' + getPoliteknik(props.NAME_2) :
              'Hover Untuk Melihat Responden');
            // }
          };

          function getPoliteknik(kota) {
            var listPoliteknik = "";
            var politeknikKota = dbKuisioner[kota];
            if (!(politeknikKota === undefined)) {
              for (var poli in politeknikKota) {
                if (poli !== 'major') {
                  listPoliteknik += (parseInt(poli) + 1) + ". " + (politeknikKota[poli].politeknik + ": " + politeknikKota[poli]
                    .total_responden + "<br>")
                }
              }
            }

            if (listPoliteknik == "") return "Tidak ada responden";

            return listPoliteknik;
          }

          info.addTo(map);

          // get color depending on dominance value
          function getColor(kota) {
            var d = "";
            if (!(dbKuisioner[kota] === undefined)) {
              d = dbKuisioner[kota]['major']
            }
            return d == 'polinema' ? '#ff0000' :
              d == 'pens' ? '#0000ff' :
              d == 'polije' ? '#A52A2A' :
              d == 'ppns' ? '#008000' :
              '#f2edc3';
          }

          function style(feature) {
            return {
              weight: 2,
              opacity: 1,
              color: 'white',
              dashArray: '3',
              fillOpacity: 0.8,
              fillColor: getColor(feature.properties.NAME_2)
            };
          }

          function highlightFeature(e) {
            var layer = e.target;

            layer.setStyle({
              weight: 5,
              color: '#666',
              dashArray: '',
              fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
              layer.bringToFront();
            }

            info.update(layer.feature.properties);
          }

          var geojson;

          function resetHighlight(e) {
            geojson.resetStyle(e.target);
            info.update();
          }

          function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
          }

          function onEachFeature(feature, layer) {
            layer.on({
              mouseover: highlightFeature,
              mouseout: resetHighlight,
              click: zoomToFeature
            });
          }

          geojson = L.geoJson(jatimKota, {
            style: style,
            onEachFeature: onEachFeature
          }).addTo(map);

          map.attributionControl.addAttribution('Data responden ketertarikan Siswa terhadap politeknik');


          var legend = L.control({
            position: 'bottomright'
          });

          legend.onAdd = function(map) {

            var div = L.DomUtil.create('div', 'info legend'),
              grades = [0, 10, 20, 50, 100, 200, 500, 1000],
              labels = [],
              from, to;

            // for (var i = 0; i < grades.length; i++) {
            //   from = grades[i];
            //   to = grades[i + 1];

            //   labels.push(
            //     '<i style="background:' + getColor(from + 1) + '"></i> ' +
            //     from + (to ? '&ndash;' + to : '+'));
            // }

            labels.push(
              '<i style="background: #ff0000"></i> ' + "Polinema");

            labels.push(
              '<i style="background: #A52A2A"></i> ' + "Polije");

            labels.push(
              '<i style="background: #0000ff"></i> ' + "PENS");

            labels.push(
              '<i style="background: #008000"></i> ' + "PPNS");

            div.innerHTML = labels.join('<br>');
            return div;
          };

          legend.addTo(map);
        </script>
        <br><br>
        <div>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" class="text-center">KOTA</th>
              <th scope="col" class="text-center">RESPONDEN</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>LAMONGAN</td>
              <td>{{ $lamonganMerge }}</td>
            </tr>
            <tr>
              <td>SUMENEP</td>
              <td>{{ $sumenepMerge }}</td>
            </tr>
            <tr>
              <td>SURABAYA</td>
              <td>{{ $surabayaMerge }}</td>
            </tr>
            <tr>
              <td>KEDIRI</td>
              <td>{{ $kediriMerge }}</td>
            </tr>
            <tr>
              <td>PAMEKASAN</td>
              <td>{{ $pamekasanMerge }}</td>
            </tr>
            <tr>
              <td>BANGKALAN</td>
              <td>{{ $bangkalanMerge }}</td>
            </tr>
            <tr>
              <td>MADIUN</td>
              <td>{{ $madiunMerge }}</td>
            </tr>
            <tr>
              <td>SIDOARJO</td>
              <td>{{ $sidoarjoMerge }}</td>
            </tr>
          </tbody>
        </table>  
      </div>
      <br>
      <div>
        <table class="table table-bordered">
            <tr>
              <th scope="col" class="text-center" rowspan="2">KOTA</th>
              <th scope="col" class="text-center" colspan="20">RESPONDEN</th>
            </tr>
            <tr>
              <th scope="col" class="text-center" colspan="5">SURVEY 1</th>
              <th scope="col" class="text-center" colspan="5">SURVEY 2</th>
              <th scope="col" class="text-center" colspan="4">CA</th>
              <th scope="col" class="text-center">INFORMASI</th>
              <th colspan="2" style="background-color: #0000ff; color: white"></th> 
              <th colspan="2" style="background-color: #ff0000; color: white"></th>
              <th colspan="2" style="background-color: #008000; color: white"></th>
              <th colspan="2" style="background-color: #A52A2A; color: white"></th>
            </tr>
          <tbody>
            <tr>
              <td >LAMONGAN</td>
              <td style="background-color: #ff0000; color: white" >{{ $lamonganPolinema }}</td>
              <td style="background-color: #0000ff; color: white" >{{ $lamonganPens }}</td>
              <td style="background-color: #A52A2A; color: white" >{{ $lamonganPolije }}</td>
              <td style="background-color: #008000; color: white" >{{ $lamonganPpns }}</td>
              <td></td>
              <td style="background-color: #ff0000; color: white" >{{ $lamonganPolinema2 }}</td>
              <td style="background-color: #0000ff; color: white" >{{ $lamonganPens2 }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #0000ff; color: white" >{{ $lamonganPensCA }}</td>
              <td style="background-color: #ff0000; color: white" >{{ $lamonganPolinemaCA }}</td>
              <td style="background-color: #008000; color: white" >{{ $lamonganPpnsCA }}</td>
              <td style="background-color: #A52A2A; color: white" >{{ $lamonganPolijeCA }}</td>
              <td>LOYAL</td>
              <td>+</td>
              <td>0.5</td>
              <td>+</td>
              <td>0.5</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>SURABAYA</td>
              <td style="background-color: #0000ff; color: white" >{{ $surabayaPens }}</td>
              <td style="background-color: #ff0000; color: white" >{{ $surabayaPolinema }}</td>
              <td style="background-color: #008000; color: white" >{{ $surabayaPpns }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #0000ff; color: white" >{{ $surabayaPens2 }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #0000ff; color: white" >{{ $surabayaPensCA }}</td>
              <td style="background-color: #008000; color: white" >{{ $surabayaPpnsCA }}</td>
              <td style="background-color: #ff0000; color: white" >{{ $surabayaPolinemaCA }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td>LOYAL</td>
              <td>+</td>
              <td>0.5</td>
              <td>+</td>
              <td>0.5</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>KEDIRI</td>
              <td style="background-color: #ff0000; color: white" >{{ $kediriPolinema }}</td>
              <td style="background-color: #0000ff; color: white" >{{ $kediriPens }}</td>
              <td style="background-color: #008000; color: white" >{{ $kediriPpns }}</td>
              <td style="background-color: #A52A2A; color: white" >{{ $kediriPolije }}</td>
              <td></td>
              <td style="background-color: #ff0000; color: white" >{{ $kediriPolinema2 }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #ff0000; color: white" >{{ $kediriPolinemaCA }}</td>
              <td style="background-color: #0000ff; color: white" >{{ $kediriPensCA }}</td>
              <td style="background-color: #008000; color: white" >{{ $kediriPpnsCA }}</td>
              <td style="background-color: #A52A2A; color: white" >{{ $kediriPolijeCA }}</td>
              <td>LOYAL</td>
              <td>+</td>
              <td>0.5</td>
              <td>+</td>
              <td>0.5</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>PAMEKASAN</td>
              <td style="background-color: #0000ff; color: white" >{{ $pamekasanPens }}</td>
              <td style="background-color: #ff0000; color: white" >{{ $pamekasanPolinema }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #0000ff; color: white" >{{ $pamekasanPensCA }}</td>
              <td style="background-color: #ff0000; color: white" >{{ $pamekasanPolinemaCA }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td>+</td>
              <td>0.5</td>
              <td>+</td>
              <td>0.5</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>BANGKALAN</td>
              <td style="background-color: #0000ff; color: white" >{{ $bangkalanPens }}</td>
              <td style="background-color: #ff0000; color: white" >{{ $bangkalanPolinema }}</td>
              <td style="background-color: #008000; color: white" >{{ $bangkalanPpns }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #008000; color: white" >{{ $bangkalanPpns }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #0000ff; color: white" >{{ $bangkalanPensCA }}</td>
              <td style="background-color: #ff0000; color: white" >{{ $bangkalanPolinemaCA }}</td>
              <td style="background-color: #008000; color: white" >{{ $bangkalanPpnsCA }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td>SWING</td>
              <td>+</td>
              <td>0.5</td>
              <td>+</td>
              <td>0.5</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>MADIUN</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #0000ff; color: white" >{{ $madiunPens2 }}</td>
              <td style="background-color: #008000; color: white" >{{ $madiunPpns2 }}</td>
              <td style="background-color: #ff0000; color: white" >{{ $madiunPolinema2 }}</td>
              <td style="background-color: #a4a7ab; color: white" ></td>
              <td></td>
              <td style="background-color: #a4a7ab; color: white" >{{ $madiunPensCA }}</td>
              <td style="background-color: #a4a7ab; color: white" >{{ $madiunPolinemaCA }}</td>
              <td style="background-color: #a4a7ab; color: white" >{{ $madiunPolijeCA }}</td>
              <td style="background-color: #a4a7ab; color: white" >{{ $madiunPpnsCA }}</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>SIDOARJO</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #0000ff; color: white" >{{ $sidoarjoPens2 }}</td>
              <td style="background-color: #ff0000; color: white" >{{ $sidoarjoPolinema2 }}</td>
              <td style="background-color: #008000; color: white" >{{ $sidoarjoPpns2 }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td style="background-color: #ff0000; color: white" >{{ $sidoarjoPolinemaCA }}</td>
              <td style="background-color: #008000; color: white" >{{ $sidoarjoPpnsCA }}</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td style="background-color: #a4a7ab; color: white" >0</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
      </section>
    </div>
  </div>

</body>

</html>
