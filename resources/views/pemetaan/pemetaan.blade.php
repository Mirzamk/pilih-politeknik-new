<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pilih Politeknik</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="bootstrap/app.css">
  
</head>
<body class="hold-transition sidebar-mini">
    <style>
      .content {max-width: 100%; width: 800px; margin: auto;}
      p.question {font-family: Arial, sans-serif;font-size:15px;color: #2E2E2E;margin-bottom:0px;}
  </style>
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background-color:#FFFF00">
    <ul class="navbar-nav">
      
      
    </ul>

  </nav>
  
  <div class="content-wrapper">
    
    <section class="content">
        <div class="col-md-12 content">
          <div class="col-md-12 content">
            <h1  style="text-align: center">Tampilan Pemetaan</h1><br>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="well col-md-12">
        <h3>Tampilan pemetaan data ketertarikan</h3>
      </div>
      <div>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" class="text-center">KOTA</th>
              <th scope="col" class="text-center">RESPONDEN</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>LAMONGAN</td>
              <td>{{ $lamongan }}</td>
            </tr>
            <tr>
              <td>SUMENEP</td>
              <td>{{ $sumenep }}</td>
            </tr>
            <tr>
              <td>SURABAYA</td>
              <td>{{ $surabaya }}</td>
            </tr>
            <tr>
              <td>KEDIRI</td>
              <td>{{ $kediri }}</td>
            </tr>
            <tr>
              <td>PAMEKASAN</td>
              <td>{{ $pamekasan }}</td>
            </tr>
            <tr>
              <td>BANGKALAN</td>
              <td>{{ $bangkalan }}</td>
            </tr>
          </tbody>
        </table>  
      </div>
      <br>
      <div>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" class="text-center">SEKOLAH</th>
              <th scope="col" class="text-center">RESPONDEN</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>SMKN 1 LAMONGAN</td>
              <td>{{ $smkn1lmgn }}</td>
            </tr>
            <tr>
              <td>SMKN 1 ARJASA</td>
              <td>{{ $smkn1arjasa }}</td>
            </tr>
            <tr>
              <td>SMKN 1 NGASEM</td>
              <td>{{ $smkn1ngasem }}</td>
            </tr>
            <tr>
              <td>SMKN 1 BRONDONG</td>
              <td>{{ $smkn1brondong }}</td>
            </tr>
            <tr>
              <td>SMKN 1 SURABAYA</td>
              <td>{{ $smkn1sby }}</td>
            </tr>
            <tr>
              <td>SMKN 3 PAMEKASAN</td>
              <td>{{ $smkn3pamekasan }}</td>
            </tr>
            <tr>
              <td>SMAN 1 LAMONGAN</td>
              <td>{{ $sman1lamongan }}</td>
            </tr>
            <tr>
              <td>SMAN 15 SURABAYA</td>
              <td>{{ $sman15surabaya }}</td>
            </tr>
            <tr>
              <td>SMAN 4 PAMEKASAN</td>
              <td>{{ $sman4pamekasan }}</td>
            </tr>
            <tr>
              <td>SMAN 2 PAMEKASAN</td>
              <td>{{ $sman2pamekasan }}</td>
            </tr>
            <tr>
              <td>SMAN 1 PACIRAN</td>
              <td>{{ $sman1paciran }}</td>
            </tr>
            <tr>
              <td>SMAN 1 SUKODADI</td>
              <td>{{ $sman1sukodadi }}</td>
            </tr>
            <tr>
              <td>SMAN 2 BANGKALAN</td>
              <td>{{ $sman2bangkalan }}</td>
            </tr>
          </tbody>
        </table>  
      </div>
      <br>
      <div>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" class="text-center">POLITEKNIK</th>
              <th scope="col" class="text-center">RESPONDEN</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>PENS</td>
              <td>{{ $pens }}</td>
            </tr>
            <tr>
              <td>PPNS</td>
              <td>{{ $ppns }}</td>
            </tr>
            <tr>
              <td>POLINEMA</td>
              <td>{{ $polinema }}</td>
            </tr>
            <tr>
              <td>POLIJE</td>
              <td>{{ $polije }}</td>
            </tr>
          </tbody>
        </table>  
      </div>
      <br>
      <div>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" class="text-center">SEKOLAH</th>
              <th scope="col" class="text-center">POLITEKNIK</th>
              <th scope="col" class="text-center">RESPONDEN</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>SMKN 1 LAMONGAN</td>
              <td>PENS</td>
              <td>{{ $smkn1lmgnpens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $smkn1lmgnppns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $smkn1lmgnpolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $smkn1lmgnpolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>SMKN 1 ARJASA</td>
              <td>PENS</td>
              <td>{{ $smkn1arjasapens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $smkn1arjasappns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $smkn1arjasapolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $smkn1arjasapolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>SMKN 1 NGASEM</td>
              <td>PENS</td>
              <td>{{ $smkn1ngasempens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $smkn1ngasemppns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $smkn1ngasempolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $smkn1ngasempolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>SMKN 1 BRONDONG</td>
              <td>PENS</td>
              <td>{{ $smkn1brondongpens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $smkn1brondongppns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $smkn1brondongpolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $smkn1brondongpolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>SMKN 1 SURABAYA</td>
              <td>PENS</td>
              <td>{{ $smkn1sbypens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $smkn1sbyppns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $smkn1sbypolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $smkn1sbypolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>SMKN 1 SURABAYA</td>
              <td>PENS</td>
              <td>{{ $smkn1sbypens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $smkn1sbyppns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $smkn1sbypolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $smkn1sbypolije }}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <br>
      <div>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col" class="text-center">KOTA</th>
              <th scope="col" class="text-center">POLITEKNIK</th>
              <th scope="col" class="text-center">RESPONDEN</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>LAMONGAN</td>
              <td>PENS</td>
              <td>{{ $lamonganPens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $lamonganPpns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $lamonganPolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $lamonganPolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>SUMENEP</td>
              <td>PENS</td>
              <td>{{ $sumenepPens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $sumenepPpns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $sumenepPolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $sumenepPolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>SURABAYA</td>
              <td>PENS</td>
              <td>{{ $surabayaPens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $surabayaPpns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $surabayaPolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $surabayaPolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>KEDIRI</td>
              <td>PENS</td>
              <td>{{ $kediriPens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $kediriPpns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $kediriPolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $kediriPolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>PAMEKASAN</td>
              <td>PENS</td>
              <td>{{ $pamekasanPens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $pamekasanPpns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $pamekasanPolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $pamekasanPolije }}</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <td>BANGKALAN</td>
              <td>PENS</td>
              <td>{{ $bangkalanPens }}</td>
            </tr>
            <tr>
              <td></td>
              <td>PPNS</td>
              <td>{{ $bangkalanPpns }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLINEMA</td>
              <td>{{ $bangkalanPolinema }}</td>
            </tr>
            <tr>
              <td></td>
              <td>POLIJE</td>
              <td>{{ $bangkalanPolije }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>
  </div>
</div>

</body>

</html>
