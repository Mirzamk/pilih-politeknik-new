-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2021 at 04:57 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pilihpoliteknik`
--

-- --------------------------------------------------------

--
-- Table structure for table `kuisioner1`
--

CREATE TABLE `kuisioner1` (
  `id_kuisioner1` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nohp` varchar(20) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `sekolah` varchar(50) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `kelas` int(11) NOT NULL,
  `politeknik` varchar(50) NOT NULL,
  `politeknik2` varchar(255) NOT NULL,
  `bidang` varchar(50) NOT NULL,
  `alasan` text NOT NULL,
  `informasi` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kuisioner1`
--

INSERT INTO `kuisioner1` (`id_kuisioner1`, `nama`, `nohp`, `email`, `sekolah`, `kota`, `kelas`, `politeknik`, `politeknik2`, `bidang`, `alasan`, `informasi`, `tanggal`) VALUES
(16, 'Mely Agustin Samilan', '085706629059', 'melyagustins19@gmail.com', 'SMKN NEGERI 1 Lamongan', 'Gresik', 10, 'polinema', 'ppns', 'Informatika', 'Karena senang', 'Feeling', '2021-07-05 22:11:48'),
(17, 'MOH. IQBAL GHIBRAN', '085235925018', 'ghibranmohiqbal@gmail.com', 'SMK NEGERI 1 ARJASA', 'ARJASA', 12, 'polinema', 'ppns', 'Tehnik Mesin', 'Karna itu salah satu politehnik yang ingin saya masukin', 'Dari guru', '2021-07-05 22:18:59'),
(18, 'Agung Widodo', '082335635078', 'agungwidodo2421@gmail.com', 'SMK Negeri 1 Lamongan', 'Lamongan', 11, 'pens', 'polinema', 'Teknik Informatika', 'Karena Mencari Tempat Kuliah Yang Dekat', 'Dari Internet', '2021-07-05 22:23:06'),
(19, 'Muhammad nur fattah', '085732542958', 'muhammadnurfattah611@gmail.com', 'Smkn 1 lamongan', 'Lamongan', 11, 'pens', 'ppns', 'Belum tahu', 'Karena keunggulannya', 'Dari pembicaraan orang', '2021-07-05 22:40:23'),
(20, 'Muhammad nur fattah', '085732542958', 'muhammadnurfattah611@gmail.com', 'Smkn1lamongan', 'Lamongan', 11, 'pens', 'ppns', 'Belum tahu', 'Karena keunggulannya', 'Dari pembicaraan orang', '2021-07-05 22:42:37'),
(21, 'Suci Fatmawati', '082131008532', 'sucifatmawati354313@gmail.com', 'SMKN 1 Lamongan', 'Lamongan', 12, 'polinema', 'pens', 'Teknik Informatika', 'Karna saya ingin meneruskan pendidikan dibidang TI', 'Dari kakak saya', '2021-07-05 23:19:25'),
(22, 'Salfa Mualifah', '0881026981539', 'Salfamualifah09@gmail.com', 'SMKN 1 lamongan', 'lamongan', 12, 'pens', 'polinema', 'D3 Teknik Telekomunikasi.', 'teknologi telekomunikasi terus berkembang tiada habisnya. ... Teknologi ini akan terus berkembang sampai masyarakat tidak lagi membutuhkan dan melakukan komunikasi.', 'dari sejumlah orang\" disekitar', '2021-07-05 23:21:11'),
(23, 'Suci Fatmawati', '082131008532', 'sucifatmawati354313@gmail.com', 'SMKN 1 Lamongan', 'Lamongan', 12, 'polinema', 'ppns', 'Teknik Informatika', 'Karna ingin meneruskan pendidikan dibidang IT', 'Dari kakak saya', '2021-07-05 23:21:13'),
(24, 'Pelangi Kartika Chandra Kirana', '085772227628', 'pelangikartika95@gmail.com', 'SMKN 1 LAMONGAN', 'Lamongan', 11, 'pens', 'polinema', 'Multimedia Broadcasting', 'ingin mengembangkan minat di bidang desain editing fotografi dan videografi', 'saudara', '2021-07-05 23:23:58'),
(25, 'Nelli krisnanti', '085719548723', 'nelli.krsnti@gmail.com', 'Smk N 1 Lamongan', 'Lamongan', 12, 'polinema', 'pens', 'Teknik informatika', 'Memilih politeknik lebih berorientasi pada dunia industri', 'Tau sendiri & internet', '2021-07-05 23:24:32'),
(26, 'Nelli krisnanti', '085719548723', 'nelli.krsnti@gmail.com', 'Smk N 1 Lamongan', 'Lamongan', 12, 'polinema', 'pens', 'Teknik informatika', 'Memilih politeknik lebih berorientasi pada dunia industri', 'Tau sendiri & internet', '2021-07-05 23:26:41'),
(27, 'Nelli krisnanti', '085719548723', 'nelli.krsnti@gmail.com', 'Smk N 1 Lamongan', 'Lamongan', 12, 'polinema', 'pens', 'Teknik informatika', 'Memilih politeknik lebih berorientasi pada dunia industri', 'Tau sendiri & internet', '2021-07-05 23:26:52'),
(28, 'Moh ulil absar abdallah', '085219134466', 'Mohulilabsar58@gmail.com', 'SMK', 'Jawa', 12, 'polije', 'pens', 'Pelayaran', 'Cita cita saya', 'Dari hp', '2021-07-05 23:41:24'),
(29, 'Moh ulil absar abdallah', '085219134466', 'Mohulilabsar58@gmail.com', 'SMK', 'Jawa', 12, 'polije', 'pens', 'Pelayaran', 'Cita cita saya', 'Dari hp', '2021-07-05 23:42:26'),
(30, 'Moh azri', '081916442039', 'Mohasriasri46@gmail.com', 'Smkn 1 arjasa', 'Sumenep', 12, 'polinema', 'polije', 'Tehnik', 'Karna mempunyai banyak peluang kerja dan gaji yg tinggi', 'Dari you tube dan saudara', '2021-07-06 00:02:57'),
(31, 'Moh azri', '081916442039', 'Mohasriasri46@gmail.com', 'Smkn 1 arjasa', 'Sumenep', 12, 'polinema', 'polije', 'Tehnik', 'Karna mempunyai banyak peluang kerja dan gaji yg tinggi', 'Dari you tube dan saudara', '2021-07-06 00:03:11'),
(32, 'Ahmad Khairur raziqin', '0853-3591-8218', 'khairurraziqin17@gmail.com', 'SMK negeri 1 arjasa', 'Sumenep', 12, 'polije', 'polinema', 'Administrasi bisnis', 'Karena saya melihat dari fakultas', 'Google', '2021-07-06 00:04:14'),
(33, 'Moh.Husni Mubarok', '089699476306', 'mhusni22mubarok@gmail.com', 'SMK N 1 SAMBENG', 'LAMONGAN', 12, 'polinema', 'pens', 'elektro', 'pingin aja', 'dari teman', '2021-07-06 00:11:53'),
(34, 'Dista Sabilla Rosa', '085746751539', 'Distasabilla@gmail.com', 'SMAN 1 Lamongan', 'Lamongan', 11, 'ppns', 'polinema', 'Teknik Elektro', 'Menurut saya peluang disana bisa masuk kerja', 'Mengunjungi tempatnya', '2021-07-06 00:16:15'),
(35, 'Ananda Amelia Nazifah', '083123009184', 'anandaamelianazifah@gmail.com', 'SMK Negeri 1 Lamongan', 'Lamongan', 12, 'pens', 'polinema', 'D3 multimedia broadcasting', 'Karena saya suka dengan photography dan saya ingin mempelajari lebih dalam tentang photography. Dan kebetulan juga di Politeknik itu menyediakan prodi multimedia broadcasting yg bergerak dalam bidang film, video, musik, photography, televisi, radio', 'Dari kakak kakak mahasiswa dari politeknik diatas', '2021-07-06 00:21:05'),
(36, 'Nisa', '081556654099', 'hidayatindierani56807@gmail.com', 'SMKN 1 Lamongan', 'Lamongan', 12, 'polije', 'polinema', 'Bahasa Inggris', 'Saya ingin mempelajari bahasa Inggris lebih dalam karena menurut saya pendidikan bahasa Inggris begitu penting dikehidupan sekarang', 'Saya tahu dari sosial media, terkadang ada beberapa politeknik yang muncul disosmed', '2021-07-06 00:21:35'),
(37, 'Nisa', '081556654099', '081556654099', 'SMKN 1 Lamongan', 'Lamongan', 12, 'polije', 'polinema', 'Bahasa Inggris', 'saya ingin mempelajari bahasa Inggris lebih dalam karena menurut saya pendidikan bahasa Inggris begitu penting dikehidupan sekarang', 'Saya tahu dari sosial media, terkadang ada beberapa politeknik yang muncul disosmed', '2021-07-06 00:25:08'),
(38, 'Muhammad Hanif Akbar', '082131021045', 'hanifulakbaro@gmail.com', 'SMA NEGERI 15 SBY', 'Surabaya', 10, 'pens', 'ppns', 'Informatika', 'Asyik seru Keren, bisa jadi hacker', 'Google,  guru,  ortu', '2021-07-06 00:26:23'),
(39, 'Muhammad Angga Pratama', '085335081963', 'anggatlogo@gmail.com', 'SMKN 1 LAMONGAN', 'Lamongan', 11, 'polinema', 'ppns', 'Teknik informatika', 'Karena jurusan ini memiliki prospek kerja yang bagus', 'Dari media sosial', '2021-07-06 00:38:31'),
(40, 'Echa raqi', '085815888761', 'Anestiaecha@gmail.com', 'Smkn 1 lamongan', 'Lamongan', 12, 'polinema', 'polinema', 'Bisnis', 'Asal', 'Firasat', '2021-07-06 00:41:21'),
(41, 'Natasya Tria s', '082141274670', 'natasyatria24@gmail.com', 'SMK Islam Bustanul Hikmah', 'Lamongan', 12, 'polinema', 'ppns', 'Ilmu administrasi', 'Karena saya berminat untuk melanjutkan di sana', 'Dari situs web dan sosmed', '2021-07-06 00:41:40'),
(42, 'Ilham Ardi', '081235374458', 'ultramancosmosku@gmail.com', 'SMAN 15 SURABAY', 'Surabaya', 11, 'polinema', 'polinema', 'Teknik Informatika', 'Karena yang berkaitan dengan digital akan terus berkembang', 'Internet', '2021-07-06 00:43:08'),
(43, 'AKMAS HASYIM', '081352085491', 'akmasljj2887@gmail.com', 'SMKN1 ARJASA', 'Jawa timur', 12, 'pens', 'pens', 'Matematika', 'Karana ingin melanjutkan pelajaran yang dapat disekolah yaitu teknik sis', 'Sekolah kami SMKN1 ARJASA', '2021-07-06 00:46:15'),
(44, 'AKMAS HASYIM', '081352085491', 'akmasljj2887@gmail.com', 'SMKN1 ARJASA', 'Jawa timur', 12, 'pens', 'pens', 'Matematika', 'Karena ingin melanjutkan pengetahuan yang telah di dapat kan disekolah', 'SMKN1 ARJASA', '2021-07-06 00:51:34'),
(45, 'Adinda Nur Al Deha', '085732573958', 'adndnral@gmail.com', 'Smk n 1 lamongan', 'Lamongan', 11, 'pens', 'polinema', 'PEns', 'Dekat dari rumah', 'Google', '2021-07-06 00:52:42'),
(46, 'Nur amalina rafiqah', '+62878-2345-2853', 'Dek engkanuramalinarafiqah@gmail.com', 'SMK negeri 1 Arjasa', 'Arjasa', 10, 'ppns', 'ppns', 'Agama', 'GK tau', 'Guru', '2021-07-06 01:00:50'),
(47, 'Nur amalina rafiqah', '+62878-2345-2853', 'Dek engkanuramalinarafiqah@gmail.com', 'SMK negeri 1 Arjasa', 'Arjasa', 10, 'ppns', 'ppns', 'Agama', 'GK tau', 'Guru', '2021-07-06 01:02:08'),
(48, 'ENNI ERNAWATI', '085875883495', 'enniernawati09@gmail.com', 'SMK NEGERI 1 SAMBENG', 'LAMONGAN', 12, 'polinema', 'pens', 'Informatika', 'Ingin mendalami teknik di univesitas tersebut.', 'Dari teman-teman yang berkuliah disana', '2021-07-06 01:11:10'),
(49, 'Miftakhul Maulida', '085645015886', 'miftamaulida220@gmail.com', 'SMKN 1 Surabaya', 'Surabaya', 11, 'pens', 'polinema', 'Teknik komputer', 'Karena politekniknya bagus, kemampuan mahasiswanya tinggi, tidak jauh dari rumah', 'Website, teman, kakak', '2021-07-06 01:13:30'),
(50, 'XENA FABIOLA', '083846454414', 'xenafabiola4@gmail.com', 'SMK NEGERI 1 SURABAYA', 'SURABAYA', 11, 'pens', 'ppns', 'Administrasi Kantor', 'Sesuai dengan Jurusan saya', 'Dari browser', '2021-07-06 01:17:05'),
(51, 'Fikrotul Nisa Utomo', '0899-9323-247', 'fikrotulnisa@gmail.com', 'SMKN 1 SURABAYA', 'Surabaya', 11, 'pens', 'ppns', 'Politeknik', 'Memilih politeknik lebih berorientasi pada dunia industri', 'Internet', '2021-07-06 01:22:33'),
(52, 'RATNA FEBRIANTI', '081336250950', 'ratnafebrianti07@gmail.com', 'SMK NEGERI 1 NGASEM', 'KEDIRI', 11, 'polinema', 'ppns', 'Jasa Boga', 'Sudah terinspirasi dadi beberapa tahun lalu. Bismillah semoga bisa Aminn', 'Sudah banyak wawasan yang saya dapat dari berbagai kalangan remaja di sekitaran saya.', '2021-07-06 01:25:15'),
(53, 'Puji Wahyu Harini', '081336286744', 'pujiwahyu623@gmail.com', 'SMA NEGERI 1 KARANGBINANGUN', 'LAMONGAN', 12, 'pens', 'polinema', 'TEKNIK INFORMATIKA', 'tertarik dan ingin belajar coding', 'sosial media dan informasi sekitar', '2021-07-06 01:30:35'),
(54, 'Eka Nur Hidayati', '085749398078', 'ekacilik29@gmail.com', 'SMK NEGERI 1 LAMONGAN', 'LAMONGAN', 12, 'polije', 'polinema', 'Manajemen Informatika', 'Karen Polije merupakan perguruan tinggi yang menyelenggarakan pendidikan vokasional dan terdapat teknik informatika', 'Dari kakak saya', '2021-07-06 01:40:19'),
(55, 'Zukhrufur Rafidin', '082337017843', 'rafidin864@gmail.com', 'SMAN 1 PACIRAN', 'Kab. Lamongan', 12, 'polinema', 'pens', 'Teknik Informatika', 'Karena utk jurusan informatikanya kualitas pendidikan bagus', 'Dari google', '2021-07-06 02:12:05'),
(56, 'Moch.ariesandy fanani', '085745837416', 'Tedyfanani6@gmail.com', 'smkn 1lmg', 'lamongan', 11, 'pens', 'polinema', '-', '-', '-', '2021-07-06 02:19:03'),
(57, 'Moch.ariesandy fanani', '085745837416', 'Tedyfanani6@gmail.com', 'smkn 1lmg', 'lamongan', 11, 'pens', 'polinema', '-', '-', '-', '2021-07-06 02:19:10'),
(58, 'Moch.ariesandy fanani', '085745837416', 'Tedyfanani6@gmail.com', 'smkn 1lmg', 'lamongan', 11, 'pens', 'polinema', '-', '-', '-', '2021-07-06 02:19:10'),
(59, 'Ida yanti', '085854735437', 'iday6087@gmail.com', 'Smkn1brondong', 'Lamongan', 12, 'polinema', 'polije', 'Menejemen bisnis', 'Karena saya  suka di bidang wirausaha dan saya ingin jadi pengusaha muda', 'Internet', '2021-07-06 02:21:50'),
(60, 'Bukhori muslim', NULL, 'bukhorimuslim301004@gmail.com', 'SMAN 1 Lamongan', 'Lamongan', 11, 'polinema', 'ppns', 'Ppns', 'Pengen tau', 'Acak', '2021-07-06 02:23:20'),
(61, 'Ida yanti', '085854735437', 'iday6087@gmail.com', 'Smkn 1brondong', 'Lamongan', 12, 'polinema', 'polije', 'Manejemen bisnis', 'Pengen jadi pengusaha', 'Internet', '2021-07-06 02:23:25'),
(62, 'ANDRIYANTO', '085645749187', 'andrikelock@gmail.com', 'SMKN 1 NGASEM', 'KEDIRI', 11, 'polinema', 'polinema', 'TKR', 'Lebih bagus', 'Dari atas', '2021-07-06 02:26:10'),
(63, 'A.RAZMAN NUR KHALIS', '085259264593', 'ebadnaban@gmail.com', 'SMK N 1 ARJASA', 'DESA ANGKATAN bukan kota', 12, 'pens', 'ppns', 'Bianis menajemen', 'Karna malang adalah kota yg nyaman dan terdekat sama asal saya', 'dari guru', '2021-07-06 02:27:55'),
(64, 'ZITTA RATIH PRAMESTHI', '085856513857', 'zittaratih@gmail.com', 'SMK NEGERI 1 NGASEM', 'KEDIRI', 12, 'polinema', 'ppns', 'Teknik sipil', 'Alasan saya memilih jurusan teknik sipil karena ingin memperdalam ilmu saya mengenai bangunan. Saya sangat tertarik dengan ilmu struktur dan konstruksi bangunan. Dan juga teknik sipil sangat berhubungan dengan dunia arsitektur. Oleh sebab itu saya ingin mempelajari juga berbagai software yang digunakan untuk mendesain dan menentukan struktur dan konstruksi suatu bangunan.\r\n\r\nAlasan lainnya yaitu karena peluang kerja para lulusan teknik sipil sangatlah luas. Dan juga lulusan teknik sipil sangatlah dibutuhkan di Indonesia yang sekarang ini masih dalam masa membangun negeri. Maka harapan saya dapat bekerja/mendapat pekerjaan dengan mudah.', 'Kakak kelas, saudara, sosial media', '2021-07-06 02:28:26'),
(65, 'Alvandi Dwi Nugroho', '085865630645', 'Fannugroho45@gmail.com', 'Smkn 1 ngasem', 'Kediri', 10, 'pens', 'ppns', 'Mesin', 'Keinginan', 'Media sosial', '2021-07-06 02:32:31'),
(66, 'anti indani najmi', '082332092523', 'indaninajmi@icloud.com', 'SMAN 1 Lamongan', 'Lamongan', 11, 'pens', 'polije', 'elektro industri', 'ikut kata hati si', 'dari kakak kelas', '2021-07-06 02:35:00'),
(67, 'Diyah Mulyono', NULL, 'Diyahmulyo@gmail.com', 'SMA Negeri 2 Pamekasan', 'Pamekasan', 12, 'pens', 'polinema', 'Teknik Informatika', 'Karena PENS merupakan poltek terbaik di Indonesia', 'Dari instagram', '2021-07-06 02:36:21'),
(68, 'Miftahus Surur', '085234267046', 'djlegend501@gmail.com', 'SMKN 1 arjasa', 'Kecamatan arjasa kabupaten sumenep', 12, 'polinema', 'pens', 'Teknik', 'Milih aja sih', 'Dari google gak sengaja walaupun masih rada tidak mengerti:)', '2021-07-06 02:37:29'),
(69, 'Nurhadi mulyono', '085335536910', 'nurhadimulyono2003@gmail.com', 'SMA NEGERI 2 Pamekasan', 'Pamekasan', 12, 'pens', 'polinema', 'Teknik mesin', 'Karena politeknik di atas termasuk politeknik terbaik se Indonesia', 'Saya mengetahui Informasi politeknik di atas dari google', '2021-07-06 02:46:33'),
(70, 'Rindy Dwi Novitasari', '0895366656893', 'rindydwinov07@gmail.com', 'SMK Negeri 1 Surabaya', 'Surabaya', 11, 'polinema', 'pens', 'Administrasi perkantoran', 'karena susuai kemampuan saya', 'dari Google', '2021-07-06 02:48:26'),
(71, 'PUTRI NEHA YATUR ROHMAH', '085692531980', 'nehaputri2604@gmail.com', 'SMKN 1 NGASEM', 'KABUPATEN KEDIRI', 12, 'polinema', 'polije', 'Teknik sipil', 'Melanjutkan jurusan di smk dan ingin mendalami', 'Dari teman, guru, dan dari google', '2021-07-06 02:49:55'),
(72, 'LISA DEWI AGUSTIN', '0857-5595-3136', 'lisadewiagustin@gmail.com', 'SMK NEGERI 1 NGASEM', 'KEDIRI', 12, 'polinema', 'polinema', 'S1 teknik sipil', 'Karena ada teknik sipilnya walaupun belum tau di sana sampai S1 atau cuma D3.', 'Dari sharing-sharing dengan teman-teman sekolah dan dari google.', '2021-07-06 02:50:59'),
(73, 'LISA DEWI AGUSTIN', '0857-5595-3136', 'lisadewiagustin@gmail.com', 'SMK NEGERI 1 NGASEM', 'KEDIRI', 12, 'polinema', 'polinema', 'S1 teknik sipil', 'Karena ada teknik sipilnya walaupun belum tau di sana sampai S1 atau cuma D3.', 'Dari sharing-sharing dengan teman-teman sekolah dan dari google.', '2021-07-06 02:51:30'),
(74, 'LISA DEWI AGUSTIN', '0857-5595-3136', 'lisadewiagustin@gmail.com', 'SMK NEGERI 1 NGASEM', 'KEDIRI', 12, 'polinema', 'polinema', 'S1 teknik sipil', 'Karena ada teknik sipilnya walaupun belum tau di sana sampai S1 atau cuma D3.', 'Dari sharing-sharing dengan teman-teman sekolah dan dari google.', '2021-07-06 02:52:17'),
(75, 'Adrika yasfin', '081330646866', 'adrikayasfin2@gmail.com', 'SMKN 1 Ngasem', 'Kediri', 11, 'polinema', 'pens', 'belum tau banyak soal politeknik', 'pernah dengar soal politeknik tersebut', 'dari media dan orang orang', '2021-07-06 02:53:14'),
(76, 'Veri adi kriswanto', '087885308818', 'ferya6422@gmail.com', 'SMKN 1 NGASEM', 'KEDIRI', 11, 'polinema', 'polije', 'Rekam medik/Gizi klinik', 'Karena menurut beberapa informasi politeknik diatas merupakan politeknik yang lumayan bagus', 'Dari beberapa internet dan beberapa orang prang sekitar', '2021-07-06 03:02:46'),
(77, 'MILZA FARIHA ALMAULIDA', '081553829297', 'milza.fariha14@gmail.com', 'SMKN 1 NGASEM', 'KEDIRI', 12, 'polinema', 'polije', 'Teknik Sipil/Bahasa Inggris', 'Karena saya mencari universitas dengan akreditasi terbaik dan universitas tersebut yang saya pilih, selain itu banyak saudara yang tinggal disana, agar memudahkan saya jika seandainya terjadi sesuatu', 'Saudara dan website-website yang pernah saya cari', '2021-07-06 03:05:03'),
(78, 'TRI YULIATI', '083830284866', 'triyuliati2004@gmail.com', 'SMKN 1 NGASEM', 'Kota Kediri', 11, 'polinema', 'pens', 'Administrasi Niaga dan D3 Teknik Elektro Industri', '* Alasan memilih POLINEMA (Politeknik Negeri Malang) => karena POLINEMA lebih berorientasi pada dunia industri. \r\n* Alasan memilih PENS (Politeknik Elektronika Negeri Surabaya) => karena PENS menyelenggarakan pendidikan vokasi / terapan yang lebih banyak berorientasi pada praktik ketimbang teori dan satu-satunya politeknik yang mengkhususkan diri pada bidang teknik elektro dan teknologi.', 'Saya mengetahui politeknik yang saya pilih dari Internet dan Informasi dari sekolah saya.', '2021-07-06 03:16:34'),
(79, 'Ana', '087759836186', 'nsfynti@gmail.com', 'SMAN 1 SUMENEP', 'Sumenep', 12, 'polije', 'polije', 'Rekam medis', 'Jalur masuknya ga terlalu susah', 'Rekomendasi sekolah dan kakak kelas', '2021-07-06 03:17:16'),
(80, 'Saimatul fitriyah', '085232029337', 'Saimatulfitriyah@gmail.com', 'SMK NEGERI 1 ARJASA', 'Jawa timur', 12, 'polije', 'polinema', 'Agribisnis', 'Karena ada program studi yang cocok pada minat saya', 'Internet', '2021-07-06 03:37:48'),
(81, 'Saimatul fitriyah', '085232029337', 'Saimatulfitriyah@gmail.com', 'SMK NEGERI 1 ARJASA', 'Jawa timur', 12, 'polije', 'polinema', 'Agribisnis', 'Karena ada program studi yang cocok pada minat saya', 'Internet', '2021-07-06 03:37:53'),
(82, 'Ahmad ferdiyanto', '085231551735', 'ahmadferdiyanto382@gmail.com', 'SMK NEGERI 1 ARJASA', 'Sumenep', 12, 'polinema', 'ppns', 'Teknik sepeda motor', 'Karena mengikuti mamauan sendiri', 'Google', '2021-07-06 03:39:47'),
(83, 'Baihaqi nazruloh', '085755683018', 'haqikeceng@gmail.com', 'SMKN 1 NGASEM', 'KAB. KEDIRI', 11, 'polinema', 'ppns', 'Teknik mesin', 'Bagus', 'Google', '2021-07-06 03:41:46'),
(84, 'Naza Azis Maya Sari', '085855150233', 'nazaazis9@gmail.com', 'Smkn 1 brondong', 'Lamongan', 12, 'polinema', 'pens', 'Teknologi informasi', 'Paling utama masalah jarak', 'Google', '2021-07-06 03:52:39'),
(85, 'Naza Azis Maya Sari', '085855150233', 'nazaazis9@gmail.com', 'Smkn 1 brondong', 'Lamongan', 12, 'polinema', 'pens', 'Teknologi informasi', 'Paling utama masalah jarak', 'Google', '2021-07-06 03:54:07'),
(86, 'Naza Azis Maya Sari', '085855150233', 'nazaazis9@gmail.com', 'Smkn 1 brondong', 'Lamongan', 12, 'polinema', 'pens', 'Teknologi informasi', 'Paling utama masalah jarak', 'Google', '2021-07-06 03:54:19'),
(87, 'Naza azis maya sari', '085855150233', 'nazaazis9@gmail.com', 'Smkn 1 Brondong', 'Lamongan', 12, 'polinema', 'pens', 'Teknik informasi', 'Jarak', 'Google', '2021-07-06 03:55:43'),
(88, 'Ganesha setiawan', '087827676393', 'setiawanganesha59@gmail.com', 'SMKN 1 SURABAYA', 'SURABAYA', 11, 'pens', 'polinema', 'TIK', 'Karena sudah menjadi impian untuk masuk ITS', 'dari jejaringan internet atau google', '2021-07-06 04:03:42'),
(89, 'Moh.aminullah', '087701352594', 'Moh. Aminullah44@gmail.com', 'SMKN 1 arjasa', 'Sumenep', 12, 'polinema', 'pens', 'Teknik dan bisnis sepeda motor', 'Saya ingin mengembangkan kemampuan saya', 'Saya mencari tahu di internet terkait politeknik', '2021-07-06 04:06:38'),
(90, 'Nova Aprillia', '085748172076', 'noprila467@gmail.com', 'Smk', 'Kediri', 12, 'polinema', 'polinema', 'Teknik', 'Ada cogan', 'Cogan', '2021-07-06 04:12:05'),
(91, 'PUTRI RINDU KINANTI', '087853441143', 'putri.rindu.kinanti05@gmail.com', 'SMKN 1 SURABAYA', 'SURABAYA', 11, 'ppns', 'pens', 'Perancangan mesin kapal', 'Saya tidak tahu karena sebelumnya saya belum ada minat ke politeknik', 'Saya mencari informasi dari sumber', '2021-07-06 04:12:39'),
(92, 'ACHMAD MARSHEL MAULANA', '08977915257', 'achmadmarshel11@gmail.com', 'SMKN 1 NGASEM', 'KABUPATEN KEDIRI', 12, 'polinema', 'polije', 'Teknik Sipil', 'Perintah orang tua', 'Internet', '2021-07-06 04:24:49'),
(93, 'FIRMAN ALIANSYAH SUBAIRI', '085233004603', 'firmanaliansyah272@gmail.com', 'Sman 4 pamekasan', 'Pamekasan', 12, 'pens', 'ppns', 'Elektronik', 'Karena saya suka', 'Teman', '2021-07-06 04:25:06'),
(94, 'Ratu', '085334727362', 'Crazyblueshine@gmail.com', 'SMAN 2 Pamekasan', 'Pamekasan', 12, 'polinema', 'polije', 'Teknik mesin', 'Saya ingin saja', 'Sosialisasi', '2021-07-06 04:35:04'),
(95, 'RIKO YOHAN MAULANA', '085749583729', 'yohanriko3@gmail.com', 'SMK N 1 NGASEM', 'KEDIRI', 12, 'polije', 'polinema', 'KEDOKTERAN', 'Ingin tahu saja', 'Teman yang sudah kuliah di sana', '2021-07-06 05:01:03'),
(96, 'DIKKAS SETIAWAN', '081330370648', 'dikkassetiawan8@gmail.com', 'SMKN 1 NGASEM', 'KEDIRI', 12, 'pens', 'polinema', 'Teknik sipil', 'Karena saya mengambil jurusan arsitektur di SMKN 1 NGASEM', 'Dari kakak kelas dan tetangga', '2021-07-06 05:13:39'),
(97, 'Ayu wulandari', '085856429433', 'wulandariayu359@gmail.com', 'Smk Ahmad Yani Sukorame', 'Lamongan', 12, 'polinema', 'pens', 'Akuntansi', 'Karena memang dari dulunya pengen kuliah di Polinema', 'Dari Internet', '2021-07-06 05:26:48'),
(98, 'Muhammad kahlil gibran', '087782063818', 'Riodwif123@gmail.com', 'SMKN 1 Surabaya', 'Surabaya', 12, 'pens', 'ppns', 'TEKNIK KOMPUTER JARINGAN', 'yah karena daerah surabaya,lebih tepatnya dekatlah', 'dari google baca baca terkebih dahulu sama memahami dikit dikit tentang PENS dan PPNS', '2021-07-06 06:18:16'),
(99, 'Novita safitri', '081515466431', 'novitasafitriae@gmail.com', 'SMKN 1 SURABAYA', 'SURABAYA', 11, 'pens', 'polinema', 'teknik perencanaan wilayah dan kota', 'ada peluang besar untuk mendapatan gaji besar, selain itu posisi yang diisi juga termasuk penting dan vital', 'dari sumber media lainnya seperti buku\" maupun internet', '2021-07-06 06:49:39'),
(100, 'Novita safitri', '081515466431', 'novitasafitriae@gmail.com', 'SMKN 1 SBY', 'SURABAYA', 11, 'pens', 'polinema', 'teknik perencanaan wilayah dan kota', 'peluang pendapatan gaji cukup besar, selain itu posisi yang diisi juga termasuk penting dan vital', 'dari sumber media lainnya seperti: buku\" maupun internet', '2021-07-06 06:53:07'),
(101, 'Yuliana', '083841771493', 'yonglexnoob@gmail.com', 'Smkn 1 surabaya', 'surabaya', 11, 'polinema', 'pens', 'OTKP', 'Minat saja', 'instagram', '2021-07-06 06:57:28'),
(102, 'Finda Dika Avita', '089644527745', 'Findadika114@gmail.com', 'SMAN 1 PACIRAN', 'Lamongan', 12, 'polinema', 'ppns', 'Manajemen bisnis', 'Karena saya ingin memperluas wawasan tentang bisnis2 didunia perkapalan.', 'Rekomendasi dari sekolah dan dari teman atau sekitar', '2021-07-06 07:00:48'),
(103, 'Achmad Maulan Niam', '085334696941', 'azmanfigon@gmail.com', 'SMKN NEGERI 1', 'Sumenep,Kangean,Angkatan', 12, 'polinema', 'ppns', 'Agribisnis Agroteknologi', 'Karna saya pikir politeknik ini kayaknya sesuai/cocok dengan kemampuan saya,tapi aku masih mau politeknik lain seperti Bisnis Manajemen.', 'Dari belajar otodidak', '2021-07-06 07:04:32'),
(104, 'Yulianti Eka Pradita', '085815781852', 'ditasajalah151@gmail.com', 'SMA PANCA MARGA 1 LAMONGAN', 'Lamongan', 12, 'polinema', 'ppns', 'IPA', 'Pengen', 'Kakak akuu', '2021-07-06 10:18:57'),
(105, 'Mirza Faris Al Arifin', '081314380823', 'alarifinmirzafaris@gmail.com', 'SMK N 1 Surabaya', 'Surabaya', 11, 'pens', 'polije', 'Teknik Komputer', 'Bagus', 'Orang tua', '2021-07-06 10:18:57'),
(106, 'Nurul Fadli', '085336069723', 'nurulfadlinurulfadli@gmail.com', 'SMKN', 'Kota Jakarta kab,sumenep.kec,Arjasa', 12, 'ppns', 'ppns', 'Politeknik', 'Cita²', 'Dari sekolah SMKN', '2021-07-06 11:08:35'),
(107, 'Nurul Fadli', '085336069723', 'nurulfadlinurulfadli@gmail.com', 'SMKN', 'Kota Jakarta kab,sumenep.kec,Arjasa', 12, 'ppns', 'ppns', 'Politeknik', 'Cita-cita', 'Dari sekolah SMKN', '2021-07-06 11:10:24'),
(108, 'Achmad Masrul Ubaidillah', '+6281359184610', 'achmadmasrul27@gmail.com', 'SMA negeri 1 Karangbinangun', 'KABUPATEN LAMONGAN', 12, 'ppns', 'ppns', 'K3', 'Karena ada jurusan k3', 'Dari media sosial', '2021-07-06 11:28:17'),
(109, 'Apriliya Novita Sari', '085707245034', 'Wahyusulistiono0@gmail.com', 'SMKN 1 Ngasem', 'Kediri', 11, 'polinema', 'polije', 'Akuntansi', 'Karena agar bisa memiliki pengalaman mengelola keuangan dengan baik', 'Dari Internet', '2021-07-06 11:47:37'),
(110, 'Apriliya Novita Sari', '085707245034', 'Wahyusulistiono0@gmail.com', 'SMKN 1 Ngasem', 'Kediri', 11, 'polinema', 'polije', 'Akuntansi', 'Agar bisa belajar mengatur keuangan dengan baik', 'Internet', '2021-07-06 11:55:15'),
(111, 'Amira Hisana Balqis Amalya', '081334872560', 'amiraamalya07@gmail.com', 'SMKN1NGASEM', 'KEDIRI', 11, 'polinema', 'pens', 'ADMINISTRASI NIAGA', 'Karena Saya ingin belajar disalah satu politeknik terkenal diatas.', 'Dari berbagai informasi digital', '2021-07-06 11:59:19'),
(112, 'Pingky Novelya M.P', '085648733916', 'pingkynovelya10otkp4@gmail.com', 'SMKN 1 SURABAYA', 'SURABAYA', 11, 'pens', 'pens', 'Manajemen', 'Karena sesuai dengan jurusan SMK saya', 'Dari google', '2021-07-06 12:12:04'),
(113, 'Sinta Amalia', '085859581681', 'amaliasinta049@gmail.com', 'SMKN 1 NGASEM', 'Kediri', 12, 'polinema', 'polinema', 'Teknik sipil', 'Karena saya ingin menguasai mengenai ilmu sipil', 'Dari google', '2021-07-06 12:50:52'),
(114, 'NATASYA FAIRUZ AMJAD', '081334512005', 'nadiahmuthiah01@gmail.com', 'SMKN 1 SURABAYA', 'SURABAYA', 11, 'pens', 'ppns', 'D4 Teknik Telekomunikasi', 'Karena ingin mengetahui lebih dalam materi atau praktek yang akan diajarkan', 'Internet', '2021-07-06 13:16:09'),
(115, 'TEGUH KURNIAWAN', '081615553842', 'heniwan123@gmail.com', 'SMK NEGERI 1 NGASEM', 'Kabupaten kediri', 12, 'polinema', 'polije', 'Tehnik sipil', 'Sebab akreditasnya baik,dan jurusanya', 'Dari alumni sekolah,sosmed', '2021-07-06 13:38:44'),
(116, 'RAHMAT HABIBI SANJOYO', '085791367734', 'habibisanjaya12@gmail.com', 'SMK NEGERI 1 NGASEM', 'Kab.Kediri', 12, 'polinema', 'pens', 'TEKNIK MESIN', 'MELANJUTKAN JURUSAN DI SMK', 'POLINEMA', '2021-07-06 13:45:24'),
(117, 'RAHMAT HABIBI SANJOYO', '085791367734', 'habibisanjaya12@gmail.com', 'SMK NEGERI 1 NGASEM', 'Kab.Kediri', 12, 'polinema', 'pens', 'TEKNIK MESIN', 'MELANJUTKAN JURUSAN DI SMK', 'POLINEMA', '2021-07-06 13:45:24'),
(118, 'MUHAMAD VICKY IBLUL AL HUDA', '081233469091', 'vickyhuda28@gmail.com', 'SMKN 1 NGASEM', 'KEDIRI', 12, 'polinema', 'polije', 'Teknik otomotif', 'Agar memperdalam keahlian', 'Dari sosmed', '2021-07-06 13:51:04'),
(119, 'MUHAMMAD ILHAM RAMADHANI', '085228934968', 'muhammadilhamramadhani13@gmail.com', 'SMKN 1 NGASEM', 'PAPAR', 12, 'polinema', 'pens', 'Teknik elektronik', 'Lebih suka ke bidang hardware', 'Di internet', '2021-07-06 13:53:38'),
(120, 'Rofiatus sholeka', '085704051649', 'Rofiatussholeka452@gmail.com', 'SMK N 1 ngasem', 'Kediri', 12, 'polinema', 'ppns', 'Gatau', 'Gatau', 'Gatau', '2021-07-06 14:10:49'),
(121, 'Nabila Jihanisrina', NULL, 'nabilajihan@gmail.com', 'SMKN 3 Pamekasan', 'Pamekasan', 12, 'pens', 'polije', 'Teknik Informatika', 'Karena bagus didekat ITS', 'dari kakak saya', '2021-07-06 14:15:32'),
(122, 'Deviatul Inge', NULL, 'ingeaghz@gmail.com', 'SMKN 3 Pamekasan', 'Pamekasan', 12, 'polinema', 'pens', 'Teknik Informatika', 'Karena salah satu politeknik yang bagus di malang', 'dari website', '2021-07-06 14:21:57'),
(123, 'Yulinda', '085159912580', 'yulindatrsnwt@gmail.com', 'Sman 1 Sukodadi', 'Lamongan', 12, 'polinema', 'pens', 'Manajemen Pemasaran', 'Karena ada program studi yang sesuai dengan passion saya', 'Dari expocampus 2021', '2021-07-06 22:51:25'),
(124, 'RADA DIAH AYU SALSABILAH', '085808997252', 'radadiah08@gmail.com', 'SMAN 1 SUKODADI', 'Lamongan', 12, 'polinema', 'pens', 'Teknik sipil', 'So be happy', 'Sosmed', '2021-07-06 23:03:49'),
(125, 'TIKA LUSIANA', '083845507620', 'tikalusiana547@gmail.com', 'SMK NEGERI 1 NGASEM', 'KEDIRI', 12, 'polinema', 'pens', 'Teknik Informatika', 'Karena saya bersekolah di jurusan Teknik Komputer dan Jaringan ( TKJ ) ,maka dari itu saya ingin melanjutkan studi yang berhubungan dengan jurusan saya.', 'INTERNET', '2021-07-06 23:22:21'),
(126, 'FATMA ANGGRAINI MU\'AROFAH', '089530249797', 'fatmaanggraini196@gmail.com', 'SMKN 1 SAMBENG', 'LAMONGAN', 12, 'pens', 'polinema', 'Teknik Informatika', 'Karena keduanya merupakan politeknik terbaik di indonesia dan sudah mencetak alumni yang sukses', 'Sosial media, serta arahan dari guru sekolah', '2021-07-07 02:58:20'),
(127, 'FITRIA ALFATIHATUL LATIFAH', '085707954727', 'fitriaalfatihatull@gmail.com', 'SMKN  1 NGASEM', 'Kediri', 11, 'polinema', 'polije', 'Arsitektur', 'karena lapangan kerja yang masih sangat lebar', 'dari internet juga dari bapak ibu gruu', '2021-07-07 06:17:48'),
(128, 'Risqa Dwi Aprilia', '085806153123', 'risqadwi25@gmail.com', 'SMKN 1 Ngasem', 'Kota kediri', 12, 'polinema', 'ppns', '-', '-', '-', '2021-07-07 11:29:13'),
(129, 'DINNI HARIANTI', '085755684020', 'dinniharianti3@gmail.com', 'SMKN 1 NGASEM', 'KABUPATEN KEDIRI', 11, 'polinema', 'pens', 'EKONOMI', 'MENAMBAH ILMU PENGETAHUAN', 'DARI INTERNET', '2021-07-07 13:27:39'),
(130, 'puspita adi nur azizatul laili', '082301252342', 'puspitaadinura.l@gmail.com', 'smkn negeri 1', 'surabaya ,wonokromo', 11, 'pens', 'ppns', 'S2 terapan teknik informatika dan komputer', 'aku ingin bisa menguasai teknik informatika dan komputer', 'browsing dari google', '2021-07-07 22:58:34'),
(131, 'Nur Aliza Suluh Nusantara', '089510034971', 'nuralizasuluhnusantara95328@gmail.com', 'SMKN 1 Ngasem', 'KEDIRI', 12, 'polinema', 'polije', 'Teknik Sipil/Bhs Inggris', 'Karena saya mencari Universitas yg akreditasi nya baik', 'Saudara, teman dan website yg pernah saya cari', '2021-07-08 04:21:43'),
(132, 'Alya febryani efanda', '087738122457', 'alyaputrinafis79@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 11, 'polinema', 'pens', 'Pajak/akuntansi', 'Alasan saya memilih politeknik di atas yaitu karena politeknik tersebut memiliki kualitas belajar&praktek yang unggul dan lokasinya juga sangat strategis', 'Saya mengetahui politeknik tersebut dari sanak saudara dan teman', '2021-07-08 07:23:53'),
(133, 'Alya febryani efanda', '087738122457', 'alyaputrinafis79@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 11, 'polinema', 'pens', 'Pajak/akuntansi', 'Alasan saya memilih politeknik di atas karena politeknik tersebut memiliki kualitas belajar&praktek yang unggul dan lokasi politeknik tersebut sangat strategis', 'Saya mengetahui politeknik tersebut dari sanak saudara dan teman', '2021-07-08 07:27:06'),
(134, 'Adinda Cahya Kamilla Firdauzi', '085335098975', 'adindackf8@gmail.com', 'SMAN 2 BANGKALAN', 'Bangkalan', 11, 'polinema', 'polije', 'manajemen', 'terpercaya', 'keluarga', '2021-07-08 08:09:40'),
(135, 'INTAN SUCI RAMADHANI PURNAMA KUNCORO', '081235304501', 'intansuciramadhanipk@gmail.com', 'SMAN 2 BANGKALAN', 'Bangkalan', 12, 'pens', 'polinema', 'Teknik sipil', 'Karena lokasinya berada di Surabaya dan akreditasi A', 'Dari guru', '2021-07-08 08:44:16'),
(136, 'INTAN SUCI RAMADHANI PURNAMA KUNCORO', '081235304501', 'intansuciramadhanipk@gmail.com', 'SMAN 2 BANGKALAN', 'Bangkalan', 12, 'pens', 'polinema', 'Teknik sipil', 'Karena berada di Surabaya dan akreditasi A', 'Dari guru', '2021-07-08 08:46:15'),
(137, 'PINGKAN AL PUNDANI', '082330836070', 'pingkanalpundani@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 12, 'pens', 'polije', 'D3 TEKNIK ELEKTRONIKA', 'Karena lab untuk praktikum sangat lengkap', 'Dari mencari identitas logo kemudian tertarik untuk mencari progam studi', '2021-07-08 08:48:46'),
(138, 'PINGKAN AL PUNDANI', '082330836070', 'pingkanalpundani@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 12, 'pens', 'polije', 'D3 TEKNIK ELEKTRONIKA', 'Karena lab untuk praktikum sangat lengkap', 'Dari mencari identitas logo kemudian tertarik untuk mencari progam studi', '2021-07-08 08:49:00'),
(139, 'PINGKAN AL PUNDANI', '082330836070', 'pingkanalpundani@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 12, 'pens', 'polije', 'D3 TEKNIK ELEKTRONIKA', 'Karena lab untuk praktikum sangat lengkap', 'Dari mencari identitas logo kemudian tertarik untuk mencari progam studi', '2021-07-08 08:49:30'),
(140, 'PINGKAN AL PUNDANI', '082330836070', 'pingkanalpundani@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 12, 'pens', 'polije', 'D3 TEKNIK ELEKTRONIKA', 'Karena lab praktikum sangat lengkap dan berkualitas', 'Dari mencari identitas logo kemudian mencari progam studinya', '2021-07-08 08:51:48'),
(141, 'PINGKAN AL PUNDANI', '082330836070', 'pingkanalpundani@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 12, 'pens', 'polije', 'D3 TEKNIK ELEKTRONIKA', 'Karena lab untuk praktikum sangat lengkap', 'Dari mencari identitas logo kemudian tertarik untuk mencari progam studi', '2021-07-08 08:52:03'),
(142, 'ELISA REFIANI', '081335090056', 'elisaelif4@gmail.com', 'SMA 2 BANGKALAN', 'BANGKALAN', 12, 'polinema', 'pens', 'Manajemen', 'Karena fakultasnya lengkap', 'Orang terdekat dan media sosial', '2021-07-08 11:08:13'),
(143, 'ELISA REFIANI', '081335090056', 'elisaelif4@gmail.com', 'SMA 2 BANGKALAN', 'BANGKALAN', 12, 'polinema', 'pens', 'Manajemen', 'Karena fakultasnya lengkap', 'Orang terdekat dan media sosial', '2021-07-08 11:08:30'),
(144, 'Jihan Laillatul Ni\'mah', '085707018532', 'Jihanlailatul02@gmail', 'Smk Islam Bustanul Hikmah', 'Lamongan', 12, 'polinema', 'ppns', 'Managemen', 'Sedikit memikat minat', 'Berdasarkan saran seseorang', '2021-07-09 02:36:25'),
(145, 'Fibriyanti Anjali', '083853162176', 'anjalifibriyanti73@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 12, 'polinema', 'polije', 'Manajemen', 'Karna sama minat', 'Dari browsing internet', '2021-07-09 07:41:33'),
(146, 'Fibriyanti Anjali', '083853162176', 'anjalifibriyanti73@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 12, 'polinema', 'polije', 'Manajemen', 'Karna sama minat', 'Dari browsing internet', '2021-07-09 07:41:34'),
(147, 'Intan Nurlaili', '085607014799', 'intannur080605@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 11, 'ppns', 'polinema', 'Teknik Keselamatan dan kesehatan kerja', 'Karena saya tertarik pada bidang kesehatan', 'Saya mengetahuinya karena kakak saya alumni dari ppns', '2021-07-14 18:25:35'),
(148, 'Mirza Millahsyafiq Khusni', '085607908022', 'mirza.khusni@gmail.com', 'SMAN 2', 'Sidoarjo', 10, 'polije', 'ppns', 'Elektronika', '4351', '42525', '2021-07-17 21:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `kuisioner2`
--

CREATE TABLE `kuisioner2` (
  `id_kuisioner2` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nohp` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `sekolah` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `kelas` int(11) NOT NULL,
  `politeknik` varchar(50) NOT NULL,
  `politeknik2` varchar(50) NOT NULL,
  `bidang` varchar(50) NOT NULL,
  `alasan` text NOT NULL,
  `informasi` text NOT NULL,
  `juara` varchar(50) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kuisioner2`
--

INSERT INTO `kuisioner2` (`id_kuisioner2`, `nama`, `nohp`, `email`, `sekolah`, `kota`, `kelas`, `politeknik`, `politeknik2`, `bidang`, `alasan`, `informasi`, `juara`, `tanggal`) VALUES
(1, 'AMELIA RAHMA NURRADLIYAH', '0895630964821', 'ameliarn24@gmail.com', 'SMAN 2 MADIUN', 'KOTA MADIUN', 10, 'polinema', 'pens', 'Teknik Sipil', 'Karena memilih dua diantara 4 berita yang tercantum hanya dua pemaparan saja', 'dari uraian diatas', 'ya', '2021-07-18 05:19:18'),
(2, 'Gagas Rahmat Saputra', '087796831950', 'gagasrahmat73@gmail.com', 'SMA 2 KOTA MADIUN', 'KOTA MADIUN', 10, 'pens', 'polije', 'Teknik Komunikasi', 'Karena Komunikasi adalaha hal yang paling dasar untuk sebuah pekerjaan, sebab komunikasi dipakai oleh seluruh pekerjaan maka dari itu paling utama yg harus saya pelajari adalah teknik komunikasi', 'Dari Cerita pengalaman seseorang di media masa', 'ya', '2021-07-18 05:21:07'),
(3, 'NORA TITHA HELSA A', '08125904471', 'noratitha1@gmail.com', 'SMAN 02 MADIUN', 'MADIUN', 10, 'pens', 'ppns', 'FARMASI  atau Perawat', 'Ingin mengobati keluarga jika sakit', 'Dari pemberitaan sekolah', 'ya', '2021-07-18 05:38:26'),
(4, 'Mevia Dwi Susanti', '0856 4541 8468', 'meviade29@gmail.com', 'SMA N 2 MADIUN', 'Madiun', 10, 'pens', 'ppns', 'teknik elektro / teknik informamstika', 'karena direkomendasikan oleh saudara yang sudah menjadi alumni dari politeknik diatas', 'dari Saudara', 'ya', '2021-07-18 05:45:43'),
(5, 'Nadila Wahyu Fenanda', '0895367358382', 'nadilawahyuf5@gmail.com', 'Sman 2 MADIUN', 'Kota madiun', 10, 'pens', 'pens', 'Teknik elektronika', 'Banyaknya penghargaan/ prestasi Yang dicapai oleh politeknik diatas. Juga akreditasi yg bagus', 'Dari internet/ sosmed', 'ya', '2021-07-18 05:52:02'),
(6, 'AVIA RIZKY WIJAYA', '0895379956244', 'aviawijaya724@gmail.com', 'SMA N 2 MADIUN', 'MADIUN', 10, 'ppns', 'ppns', 'AKUTANSI DAN PERPAJAKAN', 'KARENA CEPAT MENDAPATKAN PEKERJAAN SETELAH LULUS', 'DARI KAKAK SAYA', 'ya', '2021-07-18 05:55:46'),
(7, 'Nadila Wahyu Fenanda', '0895367358382', 'nadilawahyuf5@gmail.com', 'Sman 2 MADIUN', 'Kota madiun', 10, 'pens', 'pens', 'Teknik elektronika', 'Karena banya penghargaan/ prestasi yang dicapai. Juga akreditasi yg bagus', 'Dari internet Dan sosmed', 'ya', '2021-07-18 05:58:33'),
(8, 'AGNES NUR MAHARANI', '089501569350', 'agnesnur960@gmail.com', 'SMAN 2 MADIUN', 'MADIUN', 10, 'ppns', 'pens', 'Perkapalan', 'Karna saya minat', 'Dari media sosial', 'ya', '2021-07-18 06:03:13'),
(9, 'Dimas chandra hari murti', '088217989623', 'murtidimas6@gmail.com', 'SMAN 2 MADIUN', 'MADIUN', 10, 'polinema', 'pens', 'Teknik sipil', 'Karena kakak kuliah di polinema', 'Kakak', 'ya', '2021-07-18 06:35:12'),
(10, 'THALYTA VIUS', '08813275179', 'thalytapramesti@gmail.com', 'SMAN 2 MADIUN', 'Kota Madiun', 11, 'pens', 'ppns', 'teknik informatika', 'karena prospek kerja yang bagus', 'dari saudara yang berkuliah disana', 'ya', '2021-07-18 09:30:02'),
(11, 'Renata Aprilia Pratiwi', NULL, 'renata.pratiwii@gmail.com', 'SMA MUHAMMADIYAH 4 PORONG', 'Sidoarjo', 10, 'ppns', 'polinema', 'Mipa', 'Tidak banyak alasan', 'Dari orang sekitar yang mengerti', 'ya', '2021-07-18 09:41:34'),
(12, 'Renata Aprilia Pratiwi', '085755404643', 'renata.pratiwii@gmail.com', 'SMA MUHAMMADIYAH 4 PORONG', 'Sidoarjo', 10, 'pens', 'pens', 'Mipa', 'Yaa begituu:)', 'Dari orang sekitar', 'ya', '2021-07-18 09:43:38'),
(13, 'Dafa', '0858-9562-5866', 'dafacandipari03@gmail.com', 'SMA Muhammadiyah 4 Porong Sidoarjo', 'Sidoarjo', 11, 'pens', 'pens', 'Multimedia', 'Karna di bangku SMA ada program unggulan multi media', 'Dari teman', 'ya', '2021-07-18 09:51:16'),
(14, 'XENA FABIOLA', '083846454414', 'xenafabiola4@gmail.com', 'SMK NEGERI 1 SURABAYA', 'SURABAYA', 11, 'pens', 'ppns', 'Administrasi Kantor', 'Karena sesuai dengan Jurusan dan minat saya', 'Browser', 'ya', '2021-07-18 09:58:39'),
(15, 'Muhammad Angga Pratama', '085335081963', 'anggatlogo@gmail.com', 'SMKN 1 LAMONGAN', 'Lamongan', 11, 'polinema', 'ppns', 'Teknik informatika', 'Jurusan ini memiliki prospek kerja yang bagus', 'Dari media sosial', 'ya', '2021-07-18 10:10:19'),
(16, 'Adinda Nur Al', '085732573958', 'adndnral@gmail.com', 'SMK N 1 Lamongan', 'Lamongan', 11, 'pens', 'pens', 'Teknik informatika komputer', 'Kalau dilihat dari sumber google, PENS meraih banyak prestasi bahkan sampai tahun 2021. Ketekunan dan kegigihan mahasiswa sangat tinggi, dan mungkin didukung oleh pembimbing dan fasilitas yang memadai. Ditambah mungkin kecerdasan dan ambisi murid-murid disana sangat tinggi, jadi ada rasa ikut berambisi dan bersaing dalam studi. ya tidak menutup kemungkinan politeknik yang lain juga memiliki hal yang saya katakan tadi, tapi saya merasa lebih memilih di PENS.', 'Dari Kuisioner ini, hahaha. Kalau tidak tau dari sini, mungkin sampai sekarang cuma tau model universitas sama institut.', 'ya', '2021-07-18 10:56:01'),
(17, 'Caecilia Meiay Indriani', '081515713676', 'caeciliam06@gmail.com', 'SMAN 4 Sidoarjo', 'Sidoarjo', 12, 'pens', 'polinema', 'Teknik Informatika', 'karena saya tertarik bidang tersebut', 'sekolah', 'ya', '2021-07-18 11:07:34'),
(18, 'TEGUH KURNIAWAN', '081615553842', 'heniwan123@gmail.com', 'SMK NEGERI 1 NGASEM', 'Kabupaten kediri', 12, 'polinema', 'ppns', 'Tehnik sipil', 'Karena akreditas,dan jurusanya', 'Sosial media', 'ya', '2021-07-18 11:10:44'),
(19, 'Salsabila Savitri Lestariningrum', '+62 857-3630-6803', 'salsabilasavitri18@gmail.com', 'SMAN 2 MADIUN', 'MADIUN', 10, 'ppns', 'polinema', 'Komputer dan informatika. Ilmu Komputer. Akuntansi', 'Agar dapat cepat dapat pekerjaan', 'Saudara', 'ya', '2021-07-18 11:49:36'),
(20, 'Anisa Nur Fadilla', '082143736344', 'anisanurfadillanisa@gmail.com', 'Sma Hangtuah 5 Sidoarjo', 'sidoarjo', 12, 'pens', 'polinema', 'kedokteran', 'karena suka', 'dari informasi di sekolah', 'ya', '2021-07-18 12:24:28'),
(21, 'Anisa Nur Fadilla', '082143736344', 'anisanurfadillanisa@gmail.com', 'Sma Hangtuah 5 Sidoarjo', 'sidoarjo', 12, 'pens', 'polinema', 'kedokteran', 'karena suka', 'dari informasi di sekolah', 'ya', '2021-07-18 12:24:29'),
(22, 'Alfina Febrianti Ananda Putri', '0895342387346', 'febriantialfina395@gmail.com', 'SMA HANG TUAH 5 SIDOARJO', 'SIDOARJO', 12, 'ppns', 'polije', 'Teknik sipil', 'Ingin mencoba aja dan minat', 'Denger info dari temen', 'ya', '2021-07-18 12:26:11'),
(23, 'Sonia', '082259745964', 'soniaapraditaa@gmail.com', 'SMA HANGTUAH 5 SIDOARJO', 'Sidoarjo', 12, 'pens', 'polije', 'D4 teknik elektro industri', 'Karena tertarik dan ingin melanjutkan usaha orangtua yang dapat di pelajari dalam prodi tersebut', 'Beberapa dari senior dan melalui laman web online', 'ya', '2021-07-18 12:27:55'),
(24, 'Novian', '088996447218', 'ignatiusmarbawa@gmail.com', 'SMA hang Tuah 5', 'Sidoarjo', 12, 'polinema', 'pens', 'Teknik', 'Karena sesuai dengan kemampuan', 'Dari orang\" sekitar', 'ya', '2021-07-18 12:28:46'),
(25, 'varadilla wahyu aw', '081553821483', 'varadillawhyu@gmail.com', 'SMA HANGTUAH 5', 'Sidoarjo', 12, 'polinema', 'polije', 'teknik kimia', 'karena banyak peluang pekerjaan yg didapatkan', 'internet', 'ya', '2021-07-18 12:29:42'),
(26, 'VARHAN FIQIH N', '081907501838', 'varhanfiqih25@gmail.com', 'SMA HANG TUAH 5', 'Sidoarjo', 12, 'polinema', 'pens', 'Teknik elektro/informatika', 'Karena menarik', 'Teman', 'ya', '2021-07-18 12:30:55'),
(27, 'Ekna Grace Adelia Putri', '08993355841', 'eknagrace08@gmail.com', 'SMA Hang Tuah 5 Sidoarjo', 'Sidoarjo', 12, 'ppns', 'polinema', 'Penerbangan', 'sebagai cadangan jika tidak masuk di penerbangan', 'Internet', 'ya', '2021-07-18 12:31:49'),
(28, 'Tabitha Safira Mutia Khanza', '085806648285', 'tabithasfr01@gmail.com', 'SMA HANGTUAH 5 SIDOARJO', 'SIDOARJO', 12, 'pens', 'polinema', 'Teknik Informatika', 'karena saya tertarik dan salah satu politeknik diatas ingin saya masuki ehehe', 'google', 'ya', '2021-07-18 12:32:22'),
(29, 'Vivit Arini', '089612521664', 'vivitarini23@gmail.com', 'SMA Hang Tuah 5 Sidoarjo', 'Sidoarjo', 12, 'ppns', 'pens', 'PPNS', 'mencoba dibidang tersebut', 'Internet', 'ya', '2021-07-18 12:33:39'),
(30, 'Andrean putra wardani', '089516197335', 'putrakludan@gmail.com', 'Hangtuah 5', 'Sidoarjo', 12, 'pens', 'pens', 'Kuliah', 'Berbaik hati dan hebat', 'Enak tidak memberati', 'tidak', '2021-07-18 12:34:01'),
(31, 'Maryam Marsaa', '-', 'mayam.marsa3@gmail.com', 'SMA HANG TUAH 5', 'Sidoarjo', 12, 'polinema', 'pens', 'Akutansi', 'Ingin belajar', 'Google', 'ya', '2021-07-18 12:46:46'),
(32, 'Muhammad Ammar Hariyadi', '089612554945', 'ammarhariyadifeb@gmail.com', 'SMA HANG TUAH 5 SIDOARJO', 'Bandar Lampung', 12, 'pens', 'polinema', 'Teknik', 'Di karenakan ingin belajar lebih dalam tentang teknik', 'Dari teman', 'ya', '2021-07-18 12:53:36'),
(33, 'YUANITA DWI ANGGRAENI', '085806667513', 'yuanitaanggi09@gmail.com', 'SMA HANG TUAH 5', 'Sidoarjo', 12, 'polinema', 'pens', 'Manajemen', 'karena menarik', 'Dari promosi', 'ya', '2021-07-18 12:53:45'),
(34, 'Angelica Nailah Salsabila S', '081335794845', 'angelicanailah924@gmail.com', 'SMA Hangtuah 5 Candi', 'Sidoarjo', 12, 'polinema', 'ppns', 'CPNS', 'Karena sesuai dengan keinginan saya', 'Dari internet', 'ya', '2021-07-18 12:54:21'),
(35, 'Yuanita Dwi Anggraeni', '085806667513', 'yuanitaanggi09@gmail.com', 'SMA HANG TUAH 5', 'Sidoarjo', 12, 'polinema', 'pens', 'Manajemen', 'Karena menarik', 'Dari promosi', 'ya', '2021-07-18 12:55:31'),
(36, 'Angelica Nailah Salsabila S', '081335794845', 'angelicanailah924@gmail.com', 'SMA Hangtuah 5 Candi', 'Sidoarjo', 12, 'polinema', 'ppns', 'Akuntansi', 'Karena sesuai dengan keinginan saya dan minat saya', 'Dari internet', 'ya', '2021-07-18 12:58:36'),
(37, 'Vanya Anandio Azzahra', '081357253036', 'vanya2003anandio@gmail.com', 'SMA HANG TUAH 5', 'Sidoarjo', 12, 'polinema', 'pens', 'telekomunikasi', 'Karena saya rasa kedua politeknik tersebut sesuai dgn jurusan ips yg saya pelajari sekarang', 'guru bk', 'ya', '2021-07-18 12:59:52'),
(38, 'Farhan Firmansyah', '081947160473', 'Farhanfirmansah6@gmail.com', 'Sma hang tuah 5', 'Sidoarjo', 12, 'polinema', 'pens', 'Teknik pangan', 'Masih bingung menentukan alasan nya\r\nKarena dari dulu udah pingin milih studi itu', 'Dari google dan cari informasi di social media', 'ya', '2021-07-18 13:17:08'),
(39, 'Aisah Kartika Fitri', '08993341699', 'aisahkartika146@gmail.com', 'SMA HANG TUAH 5 SIDOARJO', 'Sidoarjo', 12, 'pens', 'polinema', 'D4  teknik informatika', 'karena ingin menambah wawasan dan memiliki prospek yang luas dan bervariasi dalam bidang pengembangan perangkat lunak', 'social media', 'ya', '2021-07-18 13:19:34'),
(40, 'Nurul Fadli', '085336069723', 'nurulfadlinurulfadli@gmail.com', 'SMKN', 'Kota Jakarta kab,sumenep.kec,Arjasa', 11, 'pens', 'pens', 'Politeknik', 'Cita²', 'Sekolah SMKN 1 arjasa', 'ya', '2021-07-18 13:27:29'),
(41, 'Nurul Fadli', '085336069723', 'nurulfadlinurulfadli@gmail.com', 'SMKN', 'Kota Jakarta kab,sumenep.kec,Arjasa', 11, 'pens', 'pens', 'Politeknik', 'Cita²', 'Sekolah SMKN 1 Arjasa', 'ya', '2021-07-18 13:29:02'),
(42, 'Nurul Fadli', '085336069723', 'nurulfadlinurulfadli@gmail.com', 'SMKN', 'Kota Jakarta kab,sumenep.kec,Arjasa', 11, 'pens', 'pens', 'Politeknik', 'Cita²', 'Sekolah SMKN 1 Arjasa', 'ya', '2021-07-18 13:30:37'),
(43, 'Audhia ade priscila', '085608211244', 'audhiaade2005@gmail.com', 'SMA N 2 MADIUN', 'KOTA MADIUN', 10, 'pens', 'polinema', 'Akuntasi', 'Biar mudah mendapatkan pekerjaan', 'Dari media sosial', 'ya', '2021-07-18 13:35:24'),
(44, 'Audhia ade priscila', '085608211244', 'audhiaade2005@gmail.com', 'SMA N 2 MADIUN', 'KOTA MADIUN', 10, 'pens', 'polinema', 'Akuntasi', 'Agar cepat mendapatkan pekerjaan', 'Dadi media sosial', 'ya', '2021-07-18 13:38:29'),
(45, 'Dian Trias Septiani', '082337868351', 'diantrias27@gmail.com', 'SMA HANG TUAH 5 SIDOARJO', 'SIDOARJO', 12, 'polinema', 'pens', 'telekomunikasi', 'karena dr segi lokasi 2 politeknik tersebut dan alamat rumah masih terjangkau, sedangkan dr segi program studi terdapat prodi yang saya inginkan.', 'dari beberapa sumber internet dan alumni.', 'ya', '2021-07-18 13:45:25'),
(46, 'Dian Trias Septiani', '082337868351', 'diantrias27@gmail.com', 'SMA HANG TUAH 5 SIDOARJO', 'SIDOARJO', 12, 'polinema', 'pens', 'telekomunikasi', 'karena dari segi lokasi kedua politeknik tersebut dgn alamat rumah masih terjangkau, sedangkan dari segi progam studi, sebab terdapat prodi yg saya inginkan.', 'dari beberapa sumber internet dan alumni.', 'ya', '2021-07-18 13:48:36'),
(47, 'Intan Nurlaili', '085607014799', 'intannur080605@gmail.com', 'SMAN 2 BANGKALAN', 'BANGKALAN', 11, 'ppns', 'pens', 'Teknik Keselamatan dan Kesehatan Kerja', 'Saya tertarik di dunia kesehatan', 'Karena kakak saya alumni dari kampus ppns jurusan dm manufaktur', 'ya', '2021-07-18 14:04:33'),
(48, 'Intan Nurlaili', '085607014799', 'intannur080605@gmail.com', 'SMAN 2 BANGKALAN', 'Bangkalan', 11, 'ppns', 'pens', 'Teknik Keselamatan dan Kesehatan Kerja', 'Karena saya tertarik di bidang kesehatan', 'Karena kakak saya alumni dari kampus ppns jurusan dm manufaktur', 'ya', '2021-07-18 14:07:28'),
(49, 'Sylvia Queen revananta Lusiana', '085708143529', 'sylviaqueenrevananta@gmail.com', 'SMA Muhammadiyah 4 Porong', 'Sidoarjo', 10, 'pens', 'ppns', 'Teknologi game', 'Karena jurusan di politeknik di atas membuat saya tertarik dan berminat untuk memasuki jurusan tersebut', 'Searching dari beberapa web yang ada di google', 'ya', '2021-07-18 14:11:09'),
(50, 'Sylvia Queen revananta Lusiana', '085708143529', 'sylviaqueenrevananta@gmail.com', 'SMA Muhammadiyah 4 Porong', 'Sidoarjo', 10, 'pens', 'ppns', 'Teknologi game', 'Karena jurusan politeknik di atas membuat saya tertarik dan berminat untuk memasuki jurusan tersebut', 'Searching dari beberapa web yang ada di google', 'ya', '2021-07-18 14:14:40'),
(51, 'Sylvia Queen revananta Lusiana', '085708143529', 'sylviaqueenrevananta@gmail.com', 'SMA Muhammadiyah 4 Porong', 'Sidoarjo', 10, 'pens', 'ppns', 'Teknologi game', 'Karena politeknik di atas membuat saya tertarik dan berminat untuk memasuki jurusan tersebut', 'Searching dari beberapa web yang ada di google', 'ya', '2021-07-18 14:17:17'),
(52, 'Sylvia Queen revananta Lusiana', '085708143529', 'sylviaqueenrevananta@gmail.com', 'SMA Muhammadiyah 4 Porong', 'Sidoarjo', 10, 'pens', 'ppns', 'Teknologi game', 'Karena politeknik di atas membuat saya tertarik dan berminat untuk memasuki jurusan tersebut', 'searching dari beberapa web yang ada di google', 'ya', '2021-07-18 14:18:33'),
(53, 'Nurma Rahmawati Efendi', '085707429267', 'nurma.rmt@gmail.com', 'SMA Hangtuah 5 Sidoarjo', 'Sidoarjo', 12, 'polinema', 'polinema', 'Radiologi', 'Coba2', 'Google', 'ya', '2021-07-18 14:39:37'),
(54, 'Griffan Qisthan Rafidayn', '08155527275', 'griffan.qisthan@gmail.com', 'SMAN 2 MADIUN', 'Madiun', 10, 'ppns', 'polinema', 'Akuntansi, Administrasi Negara', 'Minat', 'Google', 'ya', '2021-07-18 15:27:23'),
(55, 'DHEA PRAJA GUPTA', '081335657925', 'dheapg@gmail.com', 'SMAN 2 MADIUN', 'MADIUN', 10, 'ppns', 'polinema', 'Akuntasi, Administrasi negara', 'Minat', 'Membaca di internet', 'ya', '2021-07-18 15:31:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kuisioner1`
--
ALTER TABLE `kuisioner1`
  ADD PRIMARY KEY (`id_kuisioner1`);

--
-- Indexes for table `kuisioner2`
--
ALTER TABLE `kuisioner2`
  ADD PRIMARY KEY (`id_kuisioner2`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kuisioner1`
--
ALTER TABLE `kuisioner1`
  MODIFY `id_kuisioner1` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `kuisioner2`
--
ALTER TABLE `kuisioner2`
  MODIFY `id_kuisioner2` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
