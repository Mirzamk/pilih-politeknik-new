<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Map1Controller;
use App\Http\Controllers\Kuisioner1Controller;
use App\Http\Controllers\Pemetaan2Controller;
use App\Http\Controllers\PemetaanController;
use App\Http\Controllers\Kuisioner2Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/map1',[Map1Controller::class,'index'])->name('map');
Route::get('/pemetaan',[PemetaanController::class,'index']);
Route::get('/pemetaan2',[Pemetaan2Controller::class,'index']);
Route::get('/kuisioner1',[Kuisioner1Controller::class,'index']);
Route::get('/kuisioner2',[Kuisioner2Controller::class,'index']);
Route::post('/kuisioner1',[Kuisioner1Controller::class,'store']);
Route::post('/kuisioner2',[Kuisioner2Controller::class,'store']);
